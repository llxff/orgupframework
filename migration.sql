CREATE TABLE IF NOT EXISTS `sessions` (
  `session_hash` char(32) NOT NULL,
  `id_user` int(8) unsigned NOT NULL,
  `hua_md5` char(32) DEFAULT NULL,
  `create_time` char(10) DEFAULT NULL,
  `auth_type` enum('1','2','3') NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `end_time` int(10) NOT NULL,
  `exp_time` int(11) NOT NULL COMMENT 'когда кука выдохнется, 0 - при закрытии браузера',
  `browser` varchar(30) NOT NULL,
  `browser_version` decimal(8,4) NOT NULL,
  PRIMARY KEY (`session_hash`),
  KEY `fk_sessions_users1` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `reg_date` int(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` char(32) NOT NULL,
  `last_password_change_time` int(10) NOT NULL COMMENT 'дата последней смены пароля',
  `email` varchar(45) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `activated` enum('0','1') NOT NULL DEFAULT '0',
  `last_visit` int(10) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `username` (`active`,`username`),
  KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `users_rights` (
  `id_user` int(7) NOT NULL,
  `right` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user_unique_id` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` char(32) NOT NULL,
  `hua` text NOT NULL,
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `sessions`
  ADD CONSTRAINT `fk_sessions_users1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE;

