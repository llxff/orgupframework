{%- if data.getDebug -%}
    {%- for error in data.get_errors -%}
        {{ error.message }}
        hint: {{ error.help }}
        {{ data.eol|raw }}
    {%- endfor -%}
{% endif %}
{{ data.eol|raw }}