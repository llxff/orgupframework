<?php

use Orgup\Application\Logger;
use Orgup\Application\ConfigLoader;
use Orgup\Application\Runner\IndexApplication;

define( 'ROOTDIR', realpath( __DIR__.'/../' ).'/' );

require ROOTDIR.'Orgup/Application/ClassLoader.php';

$classLoader = new Orgup\Application\ClassLoader('Orgup', ROOTDIR );
$classLoader->register();

Logger::log('Create Application');

$app = new IndexApplication(new ConfigLoader());
$app->run();