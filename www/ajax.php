<?php

use Orgup\Expansions\AjaxConfigLoader;
use Orgup\Application\Runner\AjaxApplication;

define( 'ROOTDIR', realpath( __DIR__.'/../' ).'/' );

require ROOTDIR.'Orgup/Application/ClassLoader.php';
$classLoader = new Orgup\Application\ClassLoader('Orgup', ROOTDIR );
$classLoader->register();



$app = new AjaxApplication(new AjaxConfigLoader());
$app->run();