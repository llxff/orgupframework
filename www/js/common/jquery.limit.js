(function($){
    $.fn.extend({
        limit:function( limit, element ){

            var self = $(this);

            var substringFunction = function(){
                if(self.length){
                    var val = self.val();
                }

                var length = val.length;
                if ( element ) {
                    if( length > limit ){
                        $( element ).parent().css('color','red');
                    }else {
                        $( element ).parent().css('color','#888');
                    }
                }

                if( typeof element != 'undefined') {
                    if( $(element).html().length != limit - length ){
                        $(element).html( limit - length );
                    }
                }
            };

            var interval;

            self.focus(function(){
                interval = window.setInterval( substringFunction, 100 );
                if( typeof element != 'undefined') {
                    $( element ).parent().show();
                }
            });

            self.blur(function(){
                clearInterval(interval);
                substringFunction();
                if( typeof element != 'undefined') {
                    $( element ).parent().hide();
                }
            });

            substringFunction();

            return this;
        }
    })
})(jQuery);