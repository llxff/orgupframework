orgup.pages.geolocation = {

    run: function(){

        ymaps.ready(function(){
            $.gajax({
                action: 'geolocation',
                data: {
                    city: ymaps.geolocation.city,
                    latitude: ymaps.geolocation.latitude,
                    longitude: ymaps.geolocation.longitude
                }
            });
        });
    }
};