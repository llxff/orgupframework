$(document).ready(function(){

    var dbg_height = $('#dbg-wrapper').height();

    $('#open_debug, #dbg-resize').show();
    $('#dbg-wrapper').height(0);
    $('#open_debug').toggle(
        function() {
            // with fix for chrome  ,function(){$('#dbg-slider').height('inherit');}
            $('#dbg-wrapper').animate({height:dbg_height},function(){$('#dbg-slider').height('inherit');});
        },
        function () {
            $('#dbg-wrapper').animate({height: 0});
        }
    ).css('bottom', dbg_height+'px' );

    $('#dbg-resize').mousedown(function(e){

        var dbg_wrapper = $('#dbg-wrapper');
        var old_height = dbg_wrapper.height();
        var old_clientY = e.clientY;

        $(document).mousemove(function(e){

            var clientY = e.clientY;
            if ( clientY < 200 )
                clientY = 200;

            dbg_height = old_height + old_clientY - clientY;
            if ( dbg_height < 50 )
                dbg_height = 50;

            dbg_wrapper.height(dbg_height);
            $('#dbg-slider').height('inherit');
            // fix for chrome
            $('#open_debug').css('bottom', dbg_height );
            if ( sessionStorage ) {
                sessionStorage.dbgHeight = dbg_height;
            }
        });
    });

    $(document).mouseup(function(){
        $(this).off('mousemove');
    });
});