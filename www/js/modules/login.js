orgup.pages.login = {

    run: function() {

        var login_input = $('#login-table input[name="login"]');
        var password_input = $('#login-table input[name="password"]');

        var login = login_input.val();
        var password = password_input.val();

        if ( login.length == 0 ) {
            login_input.focus();
        } else {
            if ( password.length == 0 ) {
                password_input.focus();
            }
        }
    }
};