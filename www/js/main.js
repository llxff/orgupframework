orgup.ajax = {

    render_box: function( agrs, html ){
        orgup.box.replacebox( agrs.header, html[0], agrs.boxid, (agrs.overlay == '1'), agrs.boxsize );
    },

    close_box: function(agrs) {
        $('#box'+agrs.boxid).remove();
    },

    redirect: function( agrs ) {
        window.location = agrs.location;
    }
};