if ( orgup === undefined ) {
	orgup = {
		vars: {},
        pages: {},
        templates: {}
	}
}
orgup.common = {

	notificationcount: 0,
    ajaxcache: [],
    ajax_last_cache_id: 0,
    time_before_close: 4000, // in ms
    _scroll_width: 0,

    scroll_width: function( new_scroll_width, undefined ) {

        // setter
        if ( new_scroll_width !== undefined ) {
            return this._scroll_width = new_scroll_width;
        }

        // getter
        return this._scroll_width;
    },

	showmess: function( message, messtype, time, width, undefined ) {

        if ( document.getElementById('messageframe') === null ) {
    		$('body').append('<div id="messageframe"></div>');
    	}

		var messages = [];

		if ( message instanceof Array ) {
			messages = message;
		} else {
			messages[0] = message;
		}

		var html = '';
		var notification_id = ++this.notificationcount;

        html += '<div id="notification'+notification_id+'">';
		for ( var i=0; i< messages.length; i++ ){
			html += '<div class="noterr-block noterr-'+messtype+'">'+messages[i]+'</div>';
		}
        html += '</div>';

		$('#messageframe').append(html);
        if ( width ) {
            $('#messageframe').width( width );
        }

        if ( time === undefined ) {
            time = orgup.common.time_before_close;
        }

		if ( time > 0 ) {
            setTimeout(function(){
                $('#notification'+notification_id).animate({
                   opacity: 0
                }, 500).slideUp( 500, function(){$(this).remove();});
            }, time * messages.length );
        }
	},

    for_message_manage: function() {

        $('#messageframe .noterr-block').click(function(){
			$(this).remove();
		});

        setTimeout(function(){
			$('#noterr_onstart').animate({
			   opacity: 0
			},500).slideUp(500, function(){$(this).remove();});
		}, 20000 );
    },

    check_object_or_string: function( firstObj, secondObj ) {

        if ( typeof firstObj !== typeof secondObj )
            return false;

        if ( typeof firstObj == 'object' )
            return Object.equal( firstObj, secondObj );

        return firstObj === secondObj;
    },

    form_errors: function( errors ) {
        for ( var field in errors ) {
            $('[name="'+errors[field]+'"]').addClass("ui-field-empty-error").bind('focus.formerrors', function(){
                $(this).removeClass('ui-field-empty-error').unbind('focus.formerrors');
            });
        }
    },

    // высота окна браузера в текущий момент
    innerHeight: function() {
        return ( window.innerHeight ) ? window.innerHeight : $(window).height();
    },

    // сколько сейчас проскроллено
    scroll: function() {

        if ( window.scrollY !== undefined ){
            return window.scrollY;
        }

        // for ie
        else if ( document.documentElement && document.documentElement.scrollTop ) {
            return document.documentElement.scrollTop;
        }

        return 0;
    },

    drop_n_down: function( object ){

        $( object ).children(':first').click(function(){

            if ( $(document).data('drop.clicked') ) {
                $('.ui-select-drop-menu').hide().prev().data('clicked', false );
                $(document).unbind('click.drop').data('drop.clicked', false );
            }

            var tthis = $(this);

            function hide_this() {
                tthis.data('clicked', false ).next().hide();
                $(document).unbind('click.drop').data('drop.clicked', false );
            }

            if ( tthis.data('clicked') ) {
                hide_this();
            } else {
                $(document).bind('click.drop', function(){
                    hide_this();
                }).data('drop.clicked', true );
                tthis.data('clicked', true ).next().show();
            }

            return false;
        });
    }
};

orgup.box = {

    box_last_id: 0,
    box_last_zindex: 100,

    // количество открытых боксов
    open_boxes: 0,

    open_boxes_without_overlay: 0,

    // сколько было проскролено перед появлением бокса
    scr: 0,

	replacebox: function( header, content, boxid, overlay, boxsize ){

        $('#box'+boxid+' .box-header-text').html(header);
        $('#box'+boxid+' .box-inner').html(content);
        if ( boxsize ) {
            $('#box'+boxid+' .box-container').width(boxsize);
        }
        if ( overlay && !$('#box'+boxid).hasClass('box-have-overlay') ) {

            $('#box'+boxid).css('top', 0 ).addClass('box-have-overlay').height( orgup.common.innerHeight() );
            if ( this.open_boxes_without_overlay ) {
                this._overlaing();
                --this.open_boxes_without_overlay;
            }
        }
	},

    max_width: function(){
        // 30 - это запас для отступов. 15 слева и 15 справа
        var width = $('#main').width() - 30;
        return width > 200 ? width : 200;
    },

	loadbox: function ( boxtype, boxid, parameters ){

        if ( !boxid ) {
            boxid = ++this.box_last_id;
        }

        if ( document.getElementById('box'+boxid ) === null )
	        this.showbox( orgup.lang.loading, orgup.templates.loader, boxid, null, true );

		var data = 'boxtype='+boxtype+'&boxid='+boxid;

		if ( parameters !== undefined )
			data += '&'+parameters;

		$.gajax({
			data: data,
			action: 'box'
		});
	},

    showbox: function( header, content, boxid, callback, without_overlay ) {

        var box = orgup.templates.box.replace( '%boxid%', boxid );
        box = box.replace( '%header_text%', header );
        box = box.replace( '%content%', content );

        this._open_box( box, boxid, callback, without_overlay );
    },

    _open_box: function( box, boxid, callback, without_overlay ) {

        if ( !this.open_boxes) {
            this.scr = orgup.common.scroll();
        }

        if ( this.open_boxes == this.open_boxes_without_overlay && !without_overlay )
            this._overlaing();

        $('body').prepend(box);

        var box_object = $('#box'+boxid);

        if ( !without_overlay ) {

            box_object.height( orgup.common.innerHeight() ).show().addClass('box-have-overlay');

        } else {

            ++this.open_boxes_without_overlay;

            var top;

            // если нет открытых окошек, то сдвигаем на величину скролла
            if ( !this.open_boxes ) {
                top = this.scr;
            }

            // если есть, то сдвигаем окошко на пару пикселей ниже, чтобы было видно, что это уже второе окно
            else {
                top = this.open_boxes * 4;
            }

            box_object.css({
                top: top
            }).show();
        }

        box_object.css( 'z-index', ++this.box_last_zindex );

        ++this.open_boxes;

        if ( callback )
            callback();
    },

    close: function( box ) {

        if ( typeof box == 'string' ) {
            box = $('#box'+box);
        }

        if ( !box.hasClass('box-have-overlay') ) {
            --this.open_boxes_without_overlay;
        }

        box.remove();

        if ( !--this.open_boxes ) {

            $('body').css('overflow','auto');

            $('#main').css({
                top: 0,
                overflow: 'visible',
                height: 'auto'
            });

            if ( this.scr ) {
                window.scrollTo( 0, this.scr );
            }
        }
    },

    image: function( object_a ) {

        var url = $(object_a).attr('href');

        var box_id = ++this.box_last_id;
        var box = orgup.templates.image.replace( '%boxid%', box_id );

        var if_msie = function() {
            if ( $.browser.msie && $.browser.version <= 7 )
                $('.box-cell').each(function(){
                    var tthis = $(this);
                    var height = tthis.parent().height();
                    tthis.height( height );
                    var html = tthis.html();
                    tthis.html('<table style="width: 100%; height: 100%"><tr><td>'+html+'</td></tr></table>');
                });

            // здесь высчитываем максимальную ширину бокса-картинки с отступами слева и справа по 15 пикселей
            $('#box'+box_id+' .box-img-dialog').css('max-width', orgup.box.max_width() );
            $('#box'+box_id+' .box-img').click(function(){
                orgup.box.close( $(this).parent().parent().parent().parent() );
            });
        };

        this._open_box( box.replace( '%url%', url ), box_id, if_msie );
    },

    _overlaing: function() {

        $('body').css('overflow','hidden');

        $('#main').css({
            top:       -this.scr,
            overflow:  'hidden',
            height:    orgup.common.innerHeight() + this.scr
        });

        window.scrollTo( 0, 0 );
    }
};

orgup.process_response = {

	// парсер ответа
	parse_code: function( xml, data, undefined ){

        function scripts_and_styles( type, array ) {

            var i;

            $('head').find(type).each(function(){

                for ( i in array ) {

                    if ( array.hasOwnProperty(i) && array[i] == $(this).attr('src') ) {
                        delete array[i];
                    }
                }
            });
            for ( i in array ) {

                if ( !array.hasOwnProperty(i) )
                    continue;

                if ( type == 'script' ) {

                    if ( orgup.vars._awaiting === undefined ) {
                        orgup.vars._awaiting = 0;
                    }
                    ++orgup.vars._awaiting;

                    $.getScript(array[i], function(){
                        --orgup.vars._awaiting;
                    });

                } else {

                    var el = document.createElement('link');
                    el.type = 'text/css';
                    el.rel = 'stylesheet';
                    el.media = 'all';
                    el.href = array[i];
                    $('head').appendChild( el );
                }
            }
        }

        var messages = { error: [], notification: []},
            html = [],
            scripts = [],
            styles = [],
            functions = [],
            i = 0;

        // information messages
		$(xml).find('error').each(function(){
			messages.error.push( $(this).text() );
		});
		$(xml).find('message').each(function(){
			messages.notification.push( $(this).text() );
		});

        // html
		$(xml).find('html').each(function(){
			html[ $(this).attr('key') ] = $(this).text();
		});

        // scripts
        $(xml).find('script').each(function(){
            scripts.push( $(this).text() );
        });

        // styles
        $(xml).find('style').each(function(){
            styles.push( $(this).text() );
        });

        // подгружаем скрипты
        if ( scripts.length > 0 ) {
            scripts_and_styles('script', scripts );
        }

        // подгружаем скрипты
        if ( styles.length > 0 ) {
            // такой странный тип выбран для использования его в качетсве селектора в Jquery
            scripts_and_styles('link[rel="stylesheet"]', styles );
        }

        // список функций, которые должны отработать
		$(xml).find('function').each(function(){

			functions[i] = { name : $(this).find('funcname').text(), params: {} };

			$(this).find('parameter').each(function(){
				functions[i]['params'][ $(this).find('name').text() ] = $(this).find('value').text();
			});

			i++;
		});

        var answer = [ html, functions, messages ],
            cache = $(xml).find('cachetime').text();

        if ( cache && data !== undefined && data.data !== undefined ) {

            var newcacheid = ++orgup.common.ajax_last_cache_id;
            orgup.common.ajaxcache[newcacheid] = { data: data.data, response: answer };

            if ( cache != -1 ) {
                setTimeout(function() {
                    delete(orgup.common.ajaxcache[newcacheid]);
                }, cache * 1000 );
            }
        }

		// debug
        if ( orgup.vars.debug !== undefined ) {
            $('#dbg-inner').html( $(xml).find('log').text() );
        }

		return answer;
	},

	init: function( xml, data ) {
		this.start_functions( this.parse_code( xml, data ) );
	},

	start_functions: function( result ) {

        var i, type;

        if ( orgup.vars._awaiting !== undefined && orgup.vars._awaiting > 0 ) {

            setTimeout( function(){ orgup.process_response.start_functions( result )}, 40 );
            return false;
        }

        /* notifications and errors */
        for( type in result[2] ) {
            if ( result[2][type].length > 0 )
                for( i = 0; i < result[2][type].length; i++ ) {
                    orgup.common.showmess( result[2][type][i], type );
                }
        }

        /* functions */
		if ( result[1].length > 0 ) {
			for( i = 0; i < result[1].length; i++ ) {
				if ( typeof(orgup.ajax[result[1][i].name] ) == 'function' ) {
					orgup.ajax[result[1][i].name]( result[1][i].params, result[0] );
                }
			}
		}
	}
};

jQuery.extend({

	gajax: function(s, u) {

		if (s.action === u)
            return alert('prop "action" not allowed in ajax request');

        var i,
            ajax_cache = orgup.common.ajaxcache;

		if ( !('data' in s) || s.data.length === 0 ) {
			s.data = {};
			s.data.ajaxaction = s.action;
		} else if ( s.data instanceof Object ) {
			s.data.ajaxaction = s.action;
		} else {
			s.data = s.data+'&ajaxaction='+s.action;
		}

        // check cache
        for( i in ajax_cache ) {
            if ( ajax_cache.hasOwnProperty(i) && orgup.common.check_object_or_string( ajax_cache[i].data, s.data ) ) {
				orgup.process_response.start_functions( ajax_cache[i].response );
                return;
            }
        }

		var options = {
			url: '/ajax.php',
			type: 'post',
			dataType: 'xml',
			success: function(xml){
				orgup.process_response.init(xml, s );
			},
			timeout: 30000,
			error: function(s,err){
				if ( s.status == 200 && err == 'parsererror' ) {
                    orgup.common.showmess(s.responseText, 'error', 0, 700 );
				}/*
                else if ( err == 'timeout' ) {
					alert('Connection to the server was reset on timeout.');
				}
                else if ( s.status == 12029 || s.status == 0 ) {
                    alert('No connection with network.');
                }*/
			}
		};

		var possible_options = ['beforeSend', 'complete', 'success', 'data' ];

		for ( i = 0; i < possible_options.length; i++ ) {
			if ( possible_options[i] in s )
				options[possible_options[i]] = s[possible_options[i]];
		}

		$.ajax(options);
	}
});

Object.equal = function( firstObj, secondObject, u ){

    // fix for IE <= 8: Object.keys not supported
    if ( $.browser.msie !== u && $.browser.version <= 8 )
        return false;

    /* фиксим баг type of который говорит, что null это тоже объект */
    if ( typeof firstObj !== typeof secondObject || secondObject === null )
        return false;

	var keysFirstObj = Object.keys( firstObj );
	var keysSecondObject = Object.keys( secondObject );
	if ( keysFirstObj.length != keysSecondObject.length ) {
		return false;
	}
	return !keysFirstObj.filter(function( key ){
		if ( typeof firstObj[key] == "object" || Array.isArray( firstObj[key] ) ) {
			return !Object.equal(firstObj[key], secondObject[key]);
		} else {
            return firstObj[key] !== secondObject[key];
		}
	}).length;
};

$(document).ready(function(a,u){

    var body = $('body');
    var main = $('#main');
    var orgup_common = orgup.common;
    var orgup_box = orgup.box;

    // устанавливаем ширину main
    function set_main_width() {

        main.width(

            main.height() > orgup_common.innerHeight() ?

                // если есть скролл
                body.width()

                :

                // если скролла нет - то это ширина body минус ширина сролла
                body.width() - orgup_common.scroll_width() );
    }

    // определяем ширину скролла
    (function(){

        var main_width;
        var body_width;

        // есть ли скролл
        if ( main.height() > orgup.common.innerHeight() ) {
            main_width = body.width();
            body.css('overflow-y','hidden');
            body_width = body.width();
            body.css('overflow-y','auto');
        } else {
            body_width = body.width();
            body.css('overflow-y','scroll');
            main_width = body.width();
            body.css('overflow-y','auto');
        }

        orgup_common.scroll_width( body_width - main_width );

        // устанавливаем ширину main
        main.width( main_width );
    })();

    $(window).resize(function() {

        if ( orgup_box.open_boxes ) {

            var height = orgup_common.innerHeight();
            $('body > .box-have-overlay').height( height );

            main.css({
                height: height + orgup_box.scr,
                top: -orgup_box.scr,
                width: body.width() - orgup_common.scroll_width()
            });

            $('body > .box .box-img-dialog').css('max-width', orgup_box.max_width() );

        } else {
            set_main_width();
        }
    });

    // закрытие динамических окошек
    $(this).bind('click.box touchend.box', function(e){

        var target = $(e.target);
        var classes = target.attr('class');

        // если кликнули или дотронулись за пределами всплывающего окна
        if ( /box-have-overlay/.test( classes ) ) {
            orgup.box.close( target );
        }

        // если кликнули или дотронулись по крестику на всплывающем окне
        else if ( /box-close/.test( classes ) ) {
            orgup.box.close( target.parent().parent().parent().parent() );
            return false;
        }

        // если кликнули или дотронулись где угодно, когда показана картинка
        else if ( /box-cell/.test( classes ) ) {
            orgup.box.close( target.parent().parent() );
            return false;
        }
    });

    // менеджмет информационных сообщений
    orgup.common.for_message_manage();

    // запускаем требуемые функции
	if ( orgup.vars.scripts !== u ) {
        var scripts = orgup.vars.scripts;
        var script;
        for ( script in scripts ) {
            if ( orgup.pages[scripts[script]] !== u &&
                    orgup.pages[scripts[script]].run !== u &&
                    typeof( orgup.pages[scripts[script]].run ) == 'function' ) {
		        orgup.pages[scripts[script]].run();
            } else if ( orgup.vars.debug !== u ) {
                alert( 'script '+scripts[script]+' not found!');
            }
        }
	}

    // подсвечиваем ошибки в формах
    if ( orgup.vars.form_errors !== u ) {
        orgup.common.form_errors( orgup.vars.form_errors );
    }
});

$.ajaxSetup({cache:true});