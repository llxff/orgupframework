<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 06.03.12
 * Time: 16:32
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\CustomApplication;
use Orgup\Common\User;

class CustomUser extends User
{
    public function comment_key()
    {
        return substr(md5($this->getUniqueDBID().'comment_key_salt'),2,12);
    }
}
