<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 23.09.11
 * Time: 9:59
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Cache;
use \Orgup\Application\Registry;

class Cache {

    const YML_COLLECTION = 'yml';
    /**
     * @var \MongoDB
     */
    private $Mongo;
    private $yml;

    private static $instatnce;

    /**
     * @return \Orgup\Cache\Cache
     */
    public static function instance()
    {
        if(self::$instatnce == null)
            self::$instatnce = new self();

        return self::$instatnce;
    }

    private function __construct()
    {
        $this->Mongo = Registry::instance()->get('Mongo');
        $this->yml =  $this->Mongo->selectCollection(self::YML_COLLECTION);
    }

    public function clear()
    {
        $this->Mongo->dropCollection(self::YML_COLLECTION);
        $this->yml = $this->Mongo->selectCollection(self::YML_COLLECTION);
    }

    public function insertYml($uid, $yml, $filename)
    {

        $this->deleteYml($filename);
        
        $file = array(
            'md5' => $uid,
            'content' => $yml,
            'file' => $filename
        );

        return $this->yml->insert($file);
    }

    public function deleteYml($filename)
    {
        $this->yml->remove(array('file' => $filename));
    }

    public function findYml($uid)
    {
        $criteria = array('md5' => $uid);
        $parsed = $this->yml->findOne($criteria);

        if(empty($parsed['content']))
        {
            return false;
        }

        return $parsed['content'];
    }

    /**
     * @return \MongoCollection
     */
    public function getAnalyticsCon()
    {
        return $this->Mongo->selectDB('analytics_orgup')->selectCollection('common_loans_stat');
    }
}
