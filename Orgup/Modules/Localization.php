<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 05.10.11
 * Time: 12:22
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Modules;
use \Orgup\Application\Redirect;

class Localization extends IndexModuleBuilder {
    public function run() {
        $this->Routing->setcookie('lang', $this->getWayPattern(0), time()+3600*24*360);
        $way = str_replace('/'.$this->getWayPattern(0), '', $this->Ways->thispage());
        if(!$way) $way = '/';

        throw new Redirect( $way );
    }
}
