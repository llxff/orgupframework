<?php

namespace Orgup\Modules\Index;
use \Orgup\Modules\IndexModuleBuilder;

class Error403 extends IndexModuleBuilder {

    public function run() {
        if ( $this->Data->user()->imember() ) {
            header("HTTP/1.1 403 Forbidden");
        } else {
            header("HTTP/1.1 403 Access allowed only for registered users");
        }
    }
}