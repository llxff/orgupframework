<?php

namespace Orgup\Modules\Ajax;
use Orgup\Modules\AjaxModuleBuilder;

class Error404 extends AjaxModuleBuilder {
    public function run() {
        $this->Data->add_error('WRONG_RESPONSE', 'ajax');
    }
}