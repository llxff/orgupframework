<?php

namespace Orgup\Modules\Ajax;
use Orgup\Modules\AjaxModuleBuilder;

class Error403 extends AjaxModuleBuilder {

    public function run() {
        $this->Data->add_error( 'YOU_NOT_HAVE_RIGHTS', 'ajax' );
    }
}