<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 09.04.12
 * Time: 17:27
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Modules\CLI;
use Orgup\Modules\ModuleBuilder;

abstract class CLIModuleBuilder extends ModuleBuilder
{
    private $extra = [];

    public function actionsBeforeRun() {
        $this->reconnectToDatabase();
    }

    public function setExtra($extra)
    {
        $this->extra = $extra;
    }

    public function getExtra()
    {
        return $this->extra;
    }
}
