<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 09.04.12
 * Time: 17:14
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\CLIApplication;
use Orgup\Application\ConfigLoader;
use Orgup\CLIApplication\CLIRouting;

class CLIConfigLoader extends ConfigLoader
{
    const ROUTING_FILE = 'configs/clirouting.yml';

    public function getLocalization( $lang ) {
        return array();
    }
}
