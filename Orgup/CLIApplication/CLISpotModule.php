<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 09.04.12
 * Time: 16:59
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\CLIApplication;
use Orgup\Application\SpotModuleInterface;
use Orgup\Application\MainLoaderInterface;
use Orgup\Application\Exception\Config\ConfigIsBreak;
use Orgup\Application\Registry;

class CLISpotModule implements SpotModuleInterface
{
    private $routingRules;
    private $args;
    private $runtimeErrors = array();

    const ERROR_MODULE = 'error';

    private $moduleResource;
    private $moduleName;

    public function __construct( MainLoaderInterface $ConfigLoader, array $args)
    {
        $this->routingRules = $ConfigLoader->getRoutingRules();
        $this->args = CLIArgsParser::parseArgs($args);

        $this->defineModule();
        $this->initDevParams();
    }

    private function defineModule()
    {
        if(isset($this->args['job']))
        {
            $this->setJob($this->args['job']);
        }
        else
        {
            $this->runtimeErrors[] = new CLIRuntimeError('job param not found', 'set --job=job_name from clirouting');
            $this->setErrorModule();
        }
    }

    private function setJob($job)
    {
        if(!isset($this->routingRules[$job]))
        {
            $this->runtimeErrors[] = new CLIRuntimeError('job not found', 'try to add this job to clirouting or use another job');
            $this->setErrorModule();
            return ;
        }

        $this->moduleName = $job;
        $this->moduleResource = $this->routingRules[$job];

        if(!$this->checkParams($this->moduleResource))
        {
            $this->setErrorModule();
        }
    }

    private function setErrorModule()
    {
        $this->moduleResource = $this->errorModule();
        $this->moduleName = self::ERROR_MODULE;
    }

    private function errorModule()
    {
        if(!isset($this->routingRules[self::ERROR_MODULE]))
        {
            throw new ConfigIsBreak('clirouting.yml is corrupt. error section doesn\'t exists');
        }

        return $this->routingRules[self::ERROR_MODULE];
    }

    private function checkParams($moduleSettings)
    {
        if(isset($moduleSettings['params']))
        {

            if(isset($moduleSettings['params']['required']))
            {
                $requiredParamsSettings = $moduleSettings['params']['required'];

                $requiredParams = array_keys($requiredParamsSettings);

                $exists = true;

                foreach($requiredParams as $param)
                {
                    if(!isset($this->args[$param]))
                    {
                        $this->runtimeErrors[] = new CLIRuntimeError("param $param not exists", $requiredParamsSettings[$param]);
                        $exists = false;
                    }
                }

                return $exists;
            }
        }

        return true;
    }

    private function initDevParams()
    {
        if(isset($this->args['debug']) && $this->args['debug'] === true)
        {
            Registry::instance()->set('debug', true);
        }

        if(isset($this->args['dev']))
        {
            if($this->args['dev'] === true)
            {
                Registry::instance()->set('dev', true);
            }
            else
            {
                Registry::instance()->set('dev', false);
            }
        }
    }

    public function getResource()
    {
        return $this->moduleResource['resource'];
    }

    public function getModuleName()
    {
        $this->moduleName;
    }

    public function resetToModule($module_name)
    {
        $this->setJob($module_name);
    }

    public function getAllRights()
    {
        return array();
    }

    public function getModuleRights()
    {
        return array();
    }

    public function runtimeErrors()
    {
        return $this->runtimeErrors;
    }

    public function applicationArgs()
    {
        return $this->args;
    }

    public function getExpansionsSettings()
    {
        return array();
    }
    public function getExpansion()
    {
        return null;
    }
}

