<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 09.04.12
 * Time: 19:30
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\CLIApplication;
class CLIRuntimeError
{
    private $message;
    private $help;

    function __construct($message, $help = '')
    {
        $this->message = $message;
        $this->help = $help;
    }

    public function getHelp()
    {
        return $this->help;
    }

    public function getMessage()
    {
        return $this->message;
    }
}