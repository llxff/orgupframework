<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 09.04.12
 * Time: 17:31
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\CLIApplication;
use Orgup\Application\HTTPRouting;

class CLIRouting implements HTTPRouting
{
    private $params;

    function __construct($params)
    {
        $this->params = $params;
    }


    public function route_get($key)
    {
        if(!isset($this->params[$key]))
        {
            return null;
        }

        return $this->params[$key];
    }

    public function route_post($key)
    {
        return $this->route_get($key);
    }

    public function route_cookie($key)
    {
        return $this->route_get($key);
    }

    public function route_files($key)
    {
        return $this->route_get($key);
    }

    public function post()
    {
        return $this->get();
    }

    public function get()
    {
        return $this->params;
    }

    public function cookies()
    {
        return $this->get();
    }

    public function route_request($key)
    {
        return $this->route_get($key);
    }

    public function request()
    {
        return $this->get();
    }

    public function route_server($key)
    {
        if(isset($_SERVER[$key])) return $_SERVER[$key];

        return null;
    }

    public function isPostRequest()
    {
        return false;
    }
}
