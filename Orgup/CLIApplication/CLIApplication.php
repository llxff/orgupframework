<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 09.04.12
 * Time: 16:48
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\CLIApplication;
use Orgup\Application\Application;
use Orgup\Application\Rights;
use Orgup\Application\Logger;
use Orgup\Application\Registry;

abstract class CLIApplication extends Application
{
    private $exitCode = 0;

    protected function runApplication()
    {
        $Ways = $this->getWays();
        $SpotModule = $this->getSpotModule();
        Registry::instance()->set('Routing', new CLIRouting($SpotModule->applicationArgs()) );
        $this->ModuleLoader = $this->getModuleLoader( $SpotModule, $Ways );
        unset($Ways);
        Rights::instatnce( $SpotModule->getAllRights() );

        $this->ModuleLoader->init();
        $this->ModuleLoader->runModule();

        // output
        Logger::log('Start Output');


        $this->setData($this->ModuleLoader->getData());
        if($this->getData()->has_errors())
        {
            $this->exitCode = 1;
        }

        if(Registry::instance()->get('debug') || Registry::instance()->get('dev'))
        {
            $Templator = $this->getTemplator( $this->ModuleLoader->get_module_templates() );
            $Outputer = $this->getOutputer( $Templator, $this->getData());
            Logger::log('Close...');

            return $Outputer->getOutput();
        }

        return PHP_EOL;
    }

    public function exitCode()
    {
        return $this->exitCode;
    }
}
