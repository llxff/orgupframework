<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 09.04.12
 * Time: 18:53
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\CLIApplication;
use Orgup\Application\ModuleLoader;
use Orgup\Application\Logger;
use Orgup\Application\Registry;

class CLIModuleLoader extends ModuleLoader
{
    protected $defaultDataClass = '\Orgup\DataModels\CLI\CLIData';
    protected $emptyModule = '\Orgup\Modules\CLI\ErrorModule';
    /**
     * @var \Orgup\CLIApplication\CLISpotModule
     */
    protected $SpotModule;

    protected function userHaveRightsForAccessToThisPage() {
        return true;
    }

    protected function ifUserNotMemberAndNeedRights() {
        //nothing to do
    }

    public function initModule()
    {
        parent::initModule();

        $this->setExtraSettigs();
    }

    private function setExtraSettigs()
    {
        $resource = $this->SpotModule->getResource();

        if(isset($resource['extra']))
        {
            $this->getModule()->setExtra($resource['extra']);
        }
    }

    protected function loadDataObject( $DataClass ) {

        Logger::log( 'Init DataClass '.$DataClass, __FILE__, __LINE__ );

        $this->Data = new $DataClass($this->SpotModule->runtimeErrors());

        if ( Registry::instance()->get('debug') )
        {
            $this->Data->showDebugPanel();
        }
    }

    public function get_module_templates()
    {
        $resource = $this->SpotModule->getResource();

        if(isset($resource['template']))
        {
            return $resource['template'];
        }

        return null;
    }

    protected  function initExpansions()
    {
        //nothing to do
    }
}
