<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 09.04.12
 * Time: 19:12
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\CLIApplication;
use Orgup\Application\Outputer;

class CLIOutputer extends Outputer
{
    /**
     * @return string
     */
    public function getOutput()
    {
        return $this->renderTemplate($this->getTemplator()->get_main_template());
    }
}
