<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 09.04.12
 * Time: 19:04
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\CLIApplication;
use Orgup\Application\Templator;

class CLITemplator extends Templator
{
    private $main_template = 'cli/main.tpl';

    public function __construct( $template )
    {
        if ( $template !== null )
        {
            $this->set_main_template( $template );
        }
    }

    public function set_main_template( $template_name ) {
        $this->main_template = $template_name;
    }

    public function get_main_template() {
        return $this->main_template;
    }
}
