<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 22.05.12
 * Time: 19:05
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Application\Runner;
use Orgup\CLIApplication\CLIApplication;
use Orgup\Application\Registry;
use Orgup\Common\DBAccess\DBAccess;
use Orgup\CLIApplication\CLISpotModule;
use Orgup\CLIApplication\CLIModuleLoader;
use Orgup\CLIApplication\CLITemplator;
use Orgup\CLIApplication\CLIOutputer;
use Orgup\CLIApplication\CLIRouting;
use Orgup\CustomApplication\CustomUser;

class CLI extends CLIApplication
{
    protected function beforeRun()
    {
        Registry::instance()->set('User', new CustomUser);
    }

    protected function endOfApplication()
    {
        Registry::instance()->get('MongoCon')->close();
        DBAccess::sGetDB()->close();
    }

    protected function getSpotModule()
    {
        return new CLISpotModule( $this->getConfigLoader(), $_SERVER['argv']);
    }

    protected function getModuleLoader($SpotModule, $Ways)
    {
        return new CLIModuleLoader( $SpotModule, $Ways );
    }

    protected function getTemplator($templates)
    {
        return new CLITemplator( $templates );
    }

    protected function getOutputer($Templator, &$Data)
    {
        return new CLIOutputer( $Templator, $Data);
    }

}
