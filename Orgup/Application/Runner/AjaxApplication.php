<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 22.05.12
 * Time: 19:02
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Application\Runner;
use Orgup\Application\Application;
use Orgup\CustomApplication\CustomUser;
use Orgup\Application\Registry;
use Orgup\Expansions\AjaxSpotModule;
use Orgup\Expansions\AjaxTemplator;
use Orgup\Expansions\AjaxOutputer;
use Orgup\Expansions\AjaxModuleLoader;

class AjaxApplication extends Application
{

    protected function beforeRun()
    {
        parent::beforeRun();
        Registry::instance()->set('User', new CustomUser() );
    }

    protected function endOfApplication()
    {
        Registry::instance()->User()->finish_script();
        parent::endOfApplication();
    }

    protected function getSpotModule()
    {
        return new AjaxSpotModule( $this->getConfigLoader(), Registry::instance()->get('Routing') );
    }

    protected function getModuleLoader($SpotModule, $Ways)
    {
        return new AjaxModuleLoader( $SpotModule, $Ways );
    }

    protected function getTemplator($templates)
    {
        return new AjaxTemplator();
    }

    protected function getOutputer($Templator, &$Data)
    {
        return new AjaxOutputer( $Templator, $Data);
    }
}
