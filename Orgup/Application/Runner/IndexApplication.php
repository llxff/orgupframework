<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 22.05.12
 * Time: 18:58
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Application\Runner;

use Orgup\Application\Application;
use Orgup\CustomApplication\CustomUser;
use Orgup\Application\Registry;
use Orgup\Application\SpotModule;
use Orgup\Expansions\IndexModuleLoader;
use Orgup\Expansions\IndexTemplator;
use Orgup\Expansions\IndexOutputer;

class IndexApplication extends Application
{

    protected function beforeRun()
    {
        parent::beforeRun();

        Registry::instance()->set('User', new CustomUser() );
    }

    protected function endOfApplication()
    {
        Registry::instance()->User()->finish_script();
        Registry::instance()->get('MongoCon')->close();

        parent::endOfApplication();
    }

    protected function getSpotModule()
    {
        return new SpotModule( $this->getConfigLoader(), Registry::instance()->get('Path') );
    }

    protected function getModuleLoader($SpotModule, $Ways)
    {
        return new IndexModuleLoader( $SpotModule, $Ways );
    }

    protected function getTemplator($templates)
    {
        return new IndexTemplator( $templates );
    }

    protected function getOutputer($Templator, &$Data)
    {
        return new IndexOutputer( $Templator, $Data);
    }
}
