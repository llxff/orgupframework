<?php

namespace Orgup\Application;
use Orgup\Application\Exception\Config;
use \Orgup\Application\ConfigLoader\YMLLoader;
use \Orgup\Common\Localization;
use Orgup\Application\Exception\Config\ConfigIsBreak;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;

interface MainLoaderInterface {
    public function getLocalization( $lang );
    public function getRoutingRules();
    public function getExpansions();
}

class ConfigLoader implements MainLoaderInterface {

    const MAIN_CONFIG_FILE = 'configs/main.yml';
    const ROUTING_FILE = 'configs/routing.yml';
    const EXPANSIONS_FILE = 'configs/expansion.yml';

    protected $routing_rules = array();
    protected $configLoader;
    protected $mainConfig;
    protected $expansions = array();

    protected $Registry;

    public function __construct() {

        Logger::log( 'Run Application', __FILE__, __LINE__ );
        $this->Registry = Registry::instance();

        Logger::log('Load Mongo Layer');
        $this->initMongoConnection();

        $this->configLoader = YMLLoader::getLoader();

        Logger::log('Load main config');
        $this->loadMainApplicationSettings();

        Logger::log('Load routing');
        $this->loadRoutingRules();


        Logger::log('Load Expansions config');
        $this->expansions = $this->configLoader->loadFile( ROOTDIR.static::EXPANSIONS_FILE );

        Logger::log('Load DB layer');
        $this->initMainDBConnection();

        mb_internal_encoding('UTF-8');
        Logger::log('Configuration created');
    }

    private function loadMainApplicationSettings()
    {
        $this->mainConfig = $this->configLoader->loadFile( ROOTDIR.static::MAIN_CONFIG_FILE );
        $this->checkDB();
        $this->checkPath();
        $this->appendMainConfigSettingsToRegistry();
    }

    private function checkDB() {

        if (!isset( $this->mainConfig['db_access'] ) )
        {
            throw new ConfigIsBreak('define db_access path in main.yml file. There must be settings for DB connection');
        }
    }

    private function checkPath() {

        if (!isset( $this->mainConfig['HOST'] ) )
        {
            throw new ConfigIsBreak('Variable "HOST" not found in main.yml');
        }
    }

    private function appendMainConfigSettingsToRegistry() {

        foreach ( $this->mainConfig as $key => $value )
        {
            $this->Registry->set( $key, $value );
        }
    }

    private function loadRoutingRules() {
        $this->routing_rules = $this->configLoader->loadFile( ROOTDIR.static::ROUTING_FILE);
        $this->appendRoutingAndPathToRegistry();
    }

    private function appendRoutingAndPathToRegistry() {
        $Routing = new Routing();
        $this->Registry->set('Routing', $Routing );
        $this->Registry->set('Path', new Path( $Routing ) );
    }

    private function initMainDBConnection() {

        $classLoader = new ClassLoader('Doctrine', ROOTDIR.'system');
        $classLoader->register();

        $config = new Configuration();
        $config->setSQLLogger( new OrgupSQLLogger() );

        $db_config = $this->Registry->get('db_access');

        if(defined('ORGUP_TEST'))
        {
            $db_config = $this->Registry->get('db_access_test');
        }

        $this->Registry->set( 'db', DriverManager::getConnection( $db_config , $config ) );
    }

    private function initMongoConnection()
    {
        MongoDBLoader::load();
    }

    public function getRoutingRules() {
        return $this->routing_rules;
    }

    public function getLocalization( $lang ) {
        static $locale;
        if ( $locale !== null )
            return $locale;
        return $locale = new Localization( $this, $this->getLocale( $lang ), $lang);
    }

    public function getLocale( $locale ) {
        return $this->configLoader->loadFile( ROOTDIR.'Orgup/Locale/'.$locale.'.yml' );
    }

    public function getExpansions()
    {
        return $this->expansions;
    }
}