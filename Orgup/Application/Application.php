<?php

namespace Orgup\Application;
use Orgup\CustomApplication\CustomWays;

abstract class Application
{
    /**
     * @var ConfigLoader
     */
    private $ConfigLoader;
    /**
     * @var ModuleLoader
     */
    protected $ModuleLoader;

    /**
     * @var \Orgup\DataModels\Data
     */
    private $Data;

    function __construct(ConfigLoader &$ConfigLoader)
    {
        $this->ConfigLoader = $ConfigLoader;
    }

    public function run()
    {
        $this->beforeRun();
        $htmlOutput = '';
        try
        {
            $htmlOutput = $this->runApplication();
        }
        catch ( Redirect $e )
        {

            $this->endOfApplication();
            $this->redirect( $e->redirect_to() );
        }

        echo $htmlOutput;

        $this->ModuleLoader->beforeExit();

        $this->endOfApplication();
    }

    protected function runApplication()
    {
        $Ways = $this->getWays();
        $SpotModule = $this->getSpotModule();

        $this->ModuleLoader = $this->getModuleLoader( $SpotModule, $Ways );
        unset($Ways);
        Rights::instatnce( $SpotModule->getAllRights() );


        $this->ModuleLoader->init();
        $this->ModuleLoader->runModule();

        // output
        Logger::log('Start Output');

        $this->setData($this->ModuleLoader->getData());
        $this->getData()->set_locale( $this->getConfigLoader()->getLocalization( $this->getLocale()) );
        $Templator = $this->getTemplator( $this->ModuleLoader->get_module_templates() );
        $Outputer = $this->getOutputer( $Templator, $this->getData());
        Logger::log('Close...');

        return $Outputer->getOutput();
    }

    /**
     * @return \Orgup\DataModels\Data
     */
    public function & getData()
    {
        return $this->Data;
    }

    public function setData($Data)
    {
        $this->Data = $Data;
    }

    protected function getLocale()
    {
        return Registry::instance()->User()->getLocale();
    }

    protected function beforeRun()
    {
        //nothing
    }

    protected function endOfApplication()
    {
        Registry::instance()->get('db')->close();
        Logger::log( 'End of Application', __FILE__, __LINE__ );
    }

    private function redirect( $to )
    {
        header( "Location: ".$to );
        die();
    }

    /**
     * @return ConfigLoader
     */
    protected function &getConfigLoader()
    {
        return $this->ConfigLoader;
    }

    /**
     * @return CustomWays
     */
    protected function getWays()
    {
        return new CustomWays();
    }

    /**
     * @abstract
     * @return SpotModule
     */
    abstract protected function getSpotModule();

    /**
     * @abstract
     * @return ModuleLoader
     */
    abstract protected function getModuleLoader($SpotModule, $Ways);

    abstract protected function getTemplator($templates);
    /**
     * @abstract
     * @param $Templator
     * @param $Data
     * @param $Ways
     * @return Outputer
     */
    abstract protected function getOutputer($Templator, &$Data);

}