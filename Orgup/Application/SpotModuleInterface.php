<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 09.04.12
 * Time: 17:18
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Application;

interface SpotModuleInterface
{
    public function getResource();
    public function getModuleName();
    public function resetToModule( $module_name );
    public function getAllRights();
    public function getModuleRights();
}