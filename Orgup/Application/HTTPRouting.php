<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 24.04.12
 * Time: 16:01
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Application;

interface HTTPRouting
{
    public function route_get($key);
    public function route_post($key);
    public function route_cookie($key);
    public function route_files($key);
    public function post();
    public function get();
    public function cookies();
    public function route_request($key);
    public function request();
    public function isPostRequest();
}