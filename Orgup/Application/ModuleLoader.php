<?php

namespace Orgup\Application;
use \Orgup\Expansions\Ways;
use \Orgup\Application\Exception\Module;
use \Orgup\Modules\EmptyModule;
use \Orgup\Application\Exception\Expansion\ExpansionConfigIsBreak;
use Orgup\Application\Exception\Module\E404;
use Orgup\Application\Exception\Module\E403;
use Orgup\Application\Exception\UnsupportedOperation;
use Orgup\Application\Exception\Module\DataClassNotFound;
use Orgup\Application\Exception\Module\ModuleNotFound;
use Orgup\CustomApplication\CustomWays;

abstract class ModuleLoader {

    protected $SpotModule;

    /**
     * @var \Orgup\Modules\ModuleBuilder;
     */
    protected $Module;
    /**
     * @var \Orgup\DataModels\Data
     */
    protected $Data;
    protected $Ways;

    protected $defaultDataClass = '\Orgup\DataModels\Data';

    protected $emptyModule = '\Orgup\Modules\EmptyModule';
    protected $resource;
    protected $ModuleName;

    public function __construct(SpotModuleInterface $SpotModule, Ways $Ways)
    {
        Logger::log('Init ModuleLoader', __FILE__, __LINE__);

        $this->SpotModule = $SpotModule;
        $this->Ways = $Ways;

        $this->ModuleName = $SpotModule->getModuleName();
        $this->resource = $SpotModule->getResource();
    }

    public function init() {
        try
        {
            Logger::log('Init Module Expansions');

            $this->initExpansions();

            if ( is_null( $this->resource ) ) {
                Logger::log("Resource not found $this->ModuleName");
                $this->loadAnotherModule('404');
            }

            if ( $this->userHaveRightsForAccessToThisPage() )
            {
                $this->initData();
                $this->initModule();
            }
            else
            {
                $this->runAnother('403');
            }
        }
        catch ( E404 $e )
        {
            $this->runAnother('404');
        }
        catch ( E403 $e )
        {
            $this->runAnother('403');
        }
    }

    /**
     * @return \Orgup\Modules\ModuleBuilder
     */
    public function getModule()
    {
        return $this->Module;
    }

    protected function runAnother( $another )
    {
        $this->loadAnotherModule( $another );
        $this->init();
        $this->runModule();
    }

    protected function loadAnotherModule( $moduleName ) {

        Logger::log("Load another module");

        if ( $this->ModuleName == $moduleName )
        {
            throw new UnsupportedOperation("Attempt to load the module with the same name $moduleName ");
        }

        $this->ModuleName = $moduleName;
        $this->SpotModule->resetToModule( $moduleName );
        $this->resource = $this->SpotModule->getResource();
    }

    public function runModule()
    {
        try
        {
            Logger::log( 'Pre run of module '.$this->ModuleName, __FILE__, __LINE__ );
            $this->Module->actionsBeforeRun();
            Logger::log( 'Run module '.$this->ModuleName, __FILE__, __LINE__ );

            if(Registry::instance()->get('dev'))
            {
                $this->disableMySQLCache();
            }

            $this->Module->run();

            $this->logMySQLReadOperations();

            Logger::log( "After run of module $this->ModuleName", __FILE__, __LINE__ );
            $this->Module->actionsAfterRun();
        }
        catch ( E404 $e )
        {
            $this->runAnother('404');
        }
        catch ( E403 $e )
        {
            $this->runAnother('403');
        }
    }

    public function beforeExit()
    {
        $this->Module->onExit();
    }

    private function initExpansions()
    {
        $expansions = $this->SpotModule->getExpansionsSettings();
        $moduleExpansions = $this->SpotModule->getExpansion();

        if(empty($moduleExpansions)) return ;

        $Handlers = array();

        foreach($moduleExpansions as $type => $module)
        {
            foreach($module as $module=>$param)
            {
                if(isset($expansions[$type]['separate']))
                {
                    $separateExpansions = $expansions[$type]['separate'];
                }
                else
                {
                    $separateExpansions = true;
                }

                if($separateExpansions && !isset($expansions[$type]['expansions'][$module])  )
                {
                    throw new ExpansionConfigIsBreak("expansions.yml config is bad. Check it. Type `$type` not isset OR not isset `expansions` OR type `$module` not isset");
                }
                else if(!$separateExpansions)
                {
                    $expansions[$type]['expansions'][$module] = $module;
                }

                Logger::log('Add expansion: ' . $type.' -> '.$module.' | ' .$expansions[$type]['expansions'][$module]. ' & params : ' . print_r($param, true));
                $Handlers[$expansions[$type]['handler']][] = array('module' => $expansions[$type]['expansions'][$module], 'param' => $param);
            }
        }

        foreach($Handlers as $handlerClass => $params)
        {
            $Handler = new $handlerClass($params);
            $Handler->run();
        }
    }

    private function disableMySQLCache()
    {
        $db = Registry::instance()->get('db');

        $db->executeQuery('SET query_cache_type = OFF');
        $db->executeQuery('FLUSH STATUS');
    }

    private function logMySQLReadOperations()
    {
        if(Registry::instance()->get('dev'))
        {
            $db = Registry::instance()->get('db');

            $result = $db->fetchAll("SHOW SESSION STATUS LIKE 'handler_%'");

            $handlers = array();

            foreach($result as $value)
            {
                $handlers[$value['Variable_name']] = (int) $value['Value'];
            }

            if($handlers['Handler_read_key'] < $handlers['Handler_read_rnd'] || $handlers['Handler_read_key'] < $handlers['Handler_read_rnd_next'])
            {
                Logger::err('Handler_read_key : '.$handlers['Handler_read_key']);
            }
            else
            {
                Logger::good('Handler_read_key : '.$handlers['Handler_read_key']);
            }

            Logger::err('Handler_read_rnd : '.$handlers['Handler_read_rnd']);
            Logger::err('Handler_read_rnd_next : '. $handlers['Handler_read_rnd_next']);
            Logger::err('Handler_read_first : '. $handlers['Handler_read_first']);
            Logger::err('Handler_read_next : '. $handlers['Handler_read_first']);
        }
    }

    /**
     * @return \Orgup\DataModels\Data
     */
    public function getData() {

        return $this->Data;
    }

    protected function initData() {

        if ( !isset( $this->resource['data'] ) ) {
            $this->loadDataObject( $this->defaultDataClass );

        }
        else if ( class_exists($this->resource['data']) ) {
            $this->loadDataObject( $this->resource['data'] );

        }
        else {
            throw new DataClassNotFound( $this->resource['data'] );
        }
    }

    public function defaultDataClass() {
        return $this->defaultDataClass;
    }

    public function defaultModuleClass()
    {
        return $this->emptyModule;
    }

    protected function loadDataObject( $ModelName ) {

        Logger::log( 'Init DataClass '.$ModelName, __FILE__, __LINE__ );
        $this->Data = new $ModelName();

        if ( Registry::instance()->get('debug') )
        {
            $this->Data->showDebugPanel();
        }
    }

    protected function initModule()
    {

        Logger::log('Load module');

        if( !isset( $this->resource['module'] ) )
        {
            $this->loadModuleObject( $this->emptyModule );
        }
        else if ( class_exists( $this->resource['module'] ) )
        {
            $this->loadModuleObject( $this->resource['module'] );
        }
        else
        {
           throw new Module\ModuleNotFound( $this->resource['module'] );
        }
    }

    protected function loadModuleObject( $moduleName ) {

        Logger::log( 'Init Module '.$moduleName, __FILE__, __LINE__ );
        $this->Module = new $moduleName( $this->Data, Registry::instance()->get('Routing'), $this->Ways );
    }

    protected function userHaveRightsForAccessToThisPage() {

        Logger::log('Check rights');
        $right_name = $this->SpotModule->getModuleRights();

        if ( $right_name === null )
            return true;

        $User = Registry::instance()->User();

        if ( !$User->imember() ) {
            $this->ifUserNotMemberAndNeedRights();
        }

        return $User->check_this_right( $right_name );
    }

    protected function ifUserNotMemberAndNeedRights() {
        throw new Redirect( CustomWays::addParam('return_to', urlencode( $this->Ways->thispage() ), $this->Ways->login() ) );
    }

    public function get_module_templates()
    {
        return null;
    }
}