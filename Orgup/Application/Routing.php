<?php

namespace Orgup\Application;

class Routing implements HTTPRouting {

    protected $get = array();
    protected $post = array();
    protected $cookie = array();
    protected $server = array();
    protected $request;

    public function __construct(){

        $this->get = $_GET;
        $this->post = $_POST;
        $this->server = $_SERVER;
        $this->cookie = $_COOKIE;
        $this->files = $_FILES;

        $this->request = array_merge($_POST, $_GET);
    }

    public function route_get( $key ) {
        if ( isset( $this->get[$key] ) )
            return $this->get[$key];
        return null;
    }

    public function route_post( $key ) {
        if ( isset( $this->post[$key] ) )
            return $this->post[$key];
        return null;
    }

    public function route_cookie( $key ) {

        if (isset( $this->cookie[$key] ) )
            return $this->cookie[$key];
        return null;
    }

    public function route_server( $key ) {

        if (isset( $this->server[$key] ) )
            return $this->server[$key];
        return null;
    }

    public function route_files( $key ) {

        if (isset( $this->files[$key] ) )
            return $this->files[$key];
        return null;
    }

    public function unset_cookie( $cookie_name ) {
        return setcookie( $cookie_name, null, 0, '/', $_SERVER['HTTP_HOST'], false, true );
    }

    public function setcookie( $name, $value, $time = 0 ) {
        return setcookie( $name, $value, $time, '/',  $_SERVER['HTTP_HOST'], false, true );
    }

    public function post()
    {
        return $this->post;
    }

    public function get()
    {
        return $this->get;
    }

    public function cookies()
    {
        return $this->cookie;
    }

    public function add_to_file($key, array $file)
    {
        $this->files[$key] = $file;
    }

    public function route_request($key)
    {
        if(isset($this->request[$key]))
        {
            return $this->request[$key];
        }

        return null;
    }
    public function request()
    {
        return $this->request;
    }

    public function isPostRequest()
    {
        return $this->route_server('REQUEST_METHOD') == 'POST';
    }
}