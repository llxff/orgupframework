<?php

namespace Orgup\Application;
use \Orgup\Common\UserAuthDataValidator;
use Orgup\Application\Exception\Config\ConfigIsBreak;

class SpotModule implements SpotModuleInterface {

    protected $moduleResource;
    protected $routingRules;
    protected $moduleName;
    protected $moduleRights;
    protected $expansionsSettings = array();
    protected $expansions = array();
    protected $definedModule;

    private $Path;
    private $modulePattern = array();
    private $max_ladder_level = 0;

    public function __construct( MainLoaderInterface $ConfigLoader, Path $Path )
    {
	    Logger::log('Init Spot', __FILE__, __LINE__);
        $this->Path = $Path;

        $this->routingRules = $ConfigLoader->getRoutingRules();
        $this->expansionsSettings = $ConfigLoader->getExpansions();
        unset($ConfigLoader);
        $this->defineModule( $this->routingRules );

        $this->checkClearDeclaration($this->definedModule, $this->moduleResource);

        if ( $this->isPathsCountMoreThanMaxSpottedPathsOnRouting() ) {
            $this->resetModule();
        }
        unset($this->Path);
    }

    /**
     * @return null or array
     */
    public function getResource() {
        return $this->moduleResource;
    }

    /**
     * @return null or array
     */
    public function getModuleName() {
        return $this->moduleName;
    }

    /**
     * @param $moduleName
     * @return null or array
     */
    public function resetToModule( $moduleName ) {

        $this->resetModule();

        if ( isset( $this->routingRules['_'.$moduleName ]['resource'] ) ) {
            $this->setModuleResourceAndRules( $this->routingRules['_'.$moduleName ], $moduleName );
        }
    }

    public function getModulePattern() {
        return $this->modulePattern;
    }

    /**
     * @return string
     */
    public function getModuleRights() {
        return $this->moduleRights;
    }

    /**
     * @return array
     */
    public function getAllRights() {
        static $modules_rights;
        if ( $modules_rights === null ) {
	        $rules = array();
            $modules_rights = array_keys( $this->loadModulesRights( $this->routingRules, $rules ) );
        }

        return $modules_rights;
    }

    private function defineModule( &$routing_rules, $ladder_level = 0 ) {

        foreach ( $routing_rules as $module_name => &$module )
        {

            $moduleFounded = false;
            $module_name = mb_substr( $module_name, 1 );



            if ( $this->haveExtendsDeclaration( $module ) ) {
                $this->extendsResourceAndRights( $module );
            }

            // проверка на путь
            if ( $this->havePathDeclaration( $module ) ) {
                if ( $this->pathIsTheWay( $module ) ) {
                    $moduleFounded = true;
                }
            }

            // проверка на паттерн
            elseif ( $this->havePatternDeclaration( $module ) ) {

                if ( $this->patternIsTheWay( $module, $ladder_level ) ) {
                    $moduleFounded = true;
                }
            }

            // проверка имени модуля
            elseif ( $this->moduleNameIsTheWay( $module_name, $ladder_level ) ) {
                $moduleFounded = true;
            }

            if ( $moduleFounded ) {

	            // внутренний редирект
	            if ( $this->has_module_url( $module ) ) {
		            $this->set_new_url( $module );
		            $this->defineModule( $this->routingRules );
		            return;
	            }

                $this->setModuleResourceAndRules( $module, $module_name );
                $this->setExpansionsOf( $module );

                $this->max_ladder_level = ( $this->max_ladder_level > $ladder_level ) ? $this->max_ladder_level : $ladder_level;

                if( $this->isAbsoluteParamEnabled() )
                {
                    break;
                }

                // проверка субмодулей
                if ( $this->haveSubmodules( $module, $ladder_level ) ) {
                    $this->defineSubmodulesOf( $module, $ladder_level );
                }

	            return;
            }
        }
    }

    protected function haveExtendsDeclaration( $module ) {
        return ( isset( $module['extends'] ) );
    }

    protected function checkClearDeclaration(& $module, & $resource)
    {
        if(isset($module['clear']))
        {
            if(in_array('templates', $module['clear']))
            {
                $module['resource']['templates']['main'] = $resource['templates']['main'] = 'main/null.htm';
                $module['resource']['templates']['header'] = $resource['templates']['header'] = 'main/null.htm';
                $module['resource']['templates']['footer'] = $resource['templates']['footer'] = 'main/null.htm';
            }
        }
    }

    protected function extendsResourceAndRights(&$module)
    {
        $inheritedModule = $module['extends'];

        if( !isset( $this->routingRules[$inheritedModule] ) )
            throw new ConfigIsBreak("not isset inherited module $inheritedModule");

        if( !isset( $module['resource'] ) )
            $module['resource'] = array();

        if( !isset( $module['rulesname'] ) && isset($this->routingRules[$inheritedModule]['rulesname'] ) )
            $module['rulesname'] = $this->routingRules[$inheritedModule]['rulesname'];

        $this->inherit( $this->routingRules[$inheritedModule]['resource'], $module['resource'] );
    }

	private function has_module_url( $module ) {
		return isset( $module['page'] );
	}

	private function set_new_url( $module ) {

		$new_url = $module['page'];

		if ( $this->Path->get_full_path() == $new_url ) {
			throw new ConfigIsBreak('Redirecting to self');
		}

		$this->Path = new Path( $new_url );
		$this->resetModule();
	}

    private function inherit( $what, &$where )
    {
        foreach( $what as $key => $value )
        {
            if(is_array($value))
                if(isset($where[$key]))
                    $this->inherit($value, $where[$key]);
                else
                    $where[$key] = $value;

            if(!isset($where[$key]))
                $where[$key] = $value;
        }
    }

    private function havePathDeclaration( $module ) {
        return isset( $module['path'] );
    }

    private function havePatternDeclaration( $module ) {
        return isset( $module['pattern'] );
    }

    private function moduleNameIsTheWay( $module_name, $ladder_level ) {
        return ( $this->is_module_name( $module_name ) AND $this->get_ladder( $ladder_level ) == $module_name );
    }

    private function patternIsTheWay( $module, $ladder_level ) {

        $way = $this->get_ladder( $ladder_level );
        $pattern = $module['pattern'];

        if(substr_count($pattern,'f|user') > 0)
        {
            $pattern = str_replace('f|user', '('.UserAuthDataValidator::loginRegexp().')', $pattern);
        }

        if(substr_count($pattern, 'f|password') > 0)
        {
            $pattern = str_replace('f|password', '('.UserAuthDataValidator::passwordRegexp().')', $pattern);
        }

        if ( preg_match( '`^'.$pattern.'$`u', $way, $matches ) ) {
            array_shift($matches);

            foreach($matches as $value)
            {
                $this->modulePattern[] = $value;
            }

            return TRUE;
        }
        return FALSE;
    }

    private function defineSubmodulesOf( &$module, $ladder_level ) {

        ++$ladder_level;

        if ( $this->get_ladder( $ladder_level ) AND !is_null( $this->moduleResource ) ) {
            $this->defineModule( $module['submodules'], $ladder_level );
        }
    }

    private function haveSubmodules( $module, $ladder_level ) {
        return isset( $module['submodules'] ) AND is_array( $module['submodules'] ) AND $this->get_ladder( $ladder_level );
    }

    private function setModuleResourceAndRules( $module, $module_name ) {

        $this->definedModule = $module;

        if ( isset( $module['resource'] ) ) {
            $this->moduleName = $module_name;
            $this->moduleResource = $module['resource'];
            if ( isset( $module['rulesname'] ) )
                $this->moduleRights = $module['rulesname'];
        } else
            throw new ConfigIsBreak("not isset resource $module_name");
    }

    private function setExpansionsOf($module)
    {
        if(isset ($module['expansion']) )
        {
            $this->expansions = $module['expansion'];
        }
    }

    private function is_module_name( $module_name ) {
        return preg_match( '/^[_a-z]{3,40}$/', $module_name );
    }

    private function get_ladder( $ladder_level ) {
        return $this->Path->get_step_of_ladder( $ladder_level );
    }

    private function pathIsTheWay( $module ) {
        return $module['path'] == $this->Path->get_ladder_path();
    }

    private function isPathsCountMoreThanMaxSpottedPathsOnRouting()
    {
        if($this->isAbsoluteParamEnabled()) return false;

        $count_of_paths = $this->Path->get_paths_size();
        if($count_of_paths > $this->max_ladder_level + 1)
        {
            Logger::err('Count of paths more than module can have');
            return true;
        }
        else
        {
            return false;
        }
    }
    
    private function resetModule()
    {
        $this->moduleResource = null;
        $this->moduleRights = null;
        $this->moduleName = null;
        $this->expansions = array();
    }

    private function loadModulesRights( $routing_rules, &$existing_rights)
    {
        if(empty($routing_rules)) return array();
        foreach ( $routing_rules as $value ) {

            if ( isset( $value['rulesname'] ) ) {
                $existing_rights[$value['rulesname']] = 0;
            }
            if ( isset( $value['submodules'] ) ) {
                $this->loadModulesRights( $value['submodules'], $existing_rights );
            }
        }

        return $existing_rights;
    }

    private function isAbsoluteParamEnabled()
    {
        if(isset($this->definedModule['absolute']))
        {
            return $this->definedModule['absolute'];
        }

        return false;
    }

    public function getAllModules()
    {
        $modules = array();

        $this->addModule($this->routingRules, $modules);

        return $modules;
    }

    private function addModule($modules, &$modules_list)
    {
        foreach($modules as $module)
        {
            if(isset($module['resource']['module']))
            {
                if(!in_array($module['resource']['module'], $modules_list))
                {
                    $modules_list[] = $module['resource']['module'];
                }
            }

            if(isset($module['submodules']))
            {
                $this->addModule($module['submodules'], $modules_list );
            }
        }
    }

	public function getExpansion()
	{
	    return $this->expansions;
	}

    public function getExpansionsSettings()
    {
        return $this->expansionsSettings;
    }
}