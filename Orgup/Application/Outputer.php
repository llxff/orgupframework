<?php
namespace Orgup\Application;

use \Orgup\Application\ConfigLoader\YMLLoader;
use Orgup\Application\Registry;
use Orgup\Application\Logger;
use Orgup\CustomApplication\CustomWays;
use Orgup\DataModels\Data;
use Orgup\Application\Templator;

abstract class Outputer {

    const SCRIPTS = 'configs/scripts.yml';
    const STYLES = 'configs/styles.yml';
    const SCRIPTS_VERSION = 'configs/orgup_scripts.yml';
    const STYLES_VERSION = 'configs/orgup_styles.yml';

    protected $scripts = array();
    protected $styles = array();

    private $Templator;
    private $Data;
    private $Ways;

    private $templateEngine;

    public function __construct(Templator $Templator, Data &$Data) {

        Logger::log('Rending page', __FILE__, __LINE__);

        $this->Templator = $Templator;
        $this->Data = $Data;

        $this->templateEngine = $this->Templator->get_template_engine();
    }
    /**
     * @return Templator
     */
    protected function getTemplator()
    {
        return $this->Templator;
    }

    /**
     * @return \Twig_Environment
     */
    protected function getTemplateEngine()
    {
        return $this->templateEngine;
    }

    /**
     * @return \Orgup\DataModels\Data
     */
    protected function getData()
    {
        return $this->Data;
    }
    /**
     * @return Orgup\CustomApplication\CustomWays
     */
    protected function getWays()
    {
        return new CustomWays();
    }


    protected function loadSettings( $keysNeededToLoad, $baseConfig, $altConfig = null ) {

        // если скрипты и стили отключены - то сразу меняем на альтернативный вариант
        if ( $altConfig !== null && !$this->usingCompressetStylesAndJs())
        {
            $baseConfig = $altConfig;
            $altConfig = null;
        }

        $keysConfig = $this->getConfigFrom( $baseConfig );

        if ( empty( $keysConfig ) && $altConfig !== null ) {
            return $this->loadSettings( $keysNeededToLoad, $altConfig );
        }

        $filesForLoad = array();

        foreach ( $keysNeededToLoad as $key ) {

            if ( isset( $keysConfig[$key] )  )
            {

                if ( isset( $keysConfig[$key]['file'] ) )
                {
                    if ( !in_array( $keysConfig[$key]['file'], $filesForLoad ) )
                    {
                        $filesForLoad[] = $keysConfig[$key]['file'];
                    }
                }
                else {
                    Logger::err( 'In file "'.$baseConfig.'" not found key "file" for key "'.$key.'"!' );
                }
            }
            else
            {
                // если в $base_config произошла ошибка - не используем его
                if ( $altConfig !== null )
                {
                    Logger::err('Loading original styles or scripts because there is an error in file "'.$baseConfig.'"', __FILE__, __LINE__ );
                    return $this->loadSettings( $keysNeededToLoad, $altConfig );
                }

               Logger::err('Style or script "'.$key.'" not exist', __FILE__, __LINE__ );
            }
        }

        return $filesForLoad;
    }

    protected function mergeKeys( $a, $b )
    {
        return array_unique( array_merge( $a, $b ) );
    }

    protected function getConfigFrom( $file )
    {
        return YMLLoader::getLoader()->loadFile( $file );
    }

    protected function usingCompressetStylesAndJs()
    {
        return Registry::instance()->get('compressed_style_and_js');
    }

    protected function renderTemplate($template)
    {
        if(!empty($template))
        {
            return $this->getTemplateEngine()
                ->loadTemplate( $template )
                    ->render( array(
                                'data' => $this->getData(),
                                'ways' => $this->getWays(),
                                'lang' => $this->getData()->getLang()
                            ) );
        }

        return '';
    }

    /**
     * @abstract
     * @return string
     */
    abstract public function getOutput();
}