<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 28.05.12
 * Time: 19:27
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Expansions;

class ModGenerator
{
    /**
     * @static
     * @param string $type Post or Get
     * @param string $accessType
     */
    public static function generateFields($type = 'Post', $accessType = 'private')
    {
        foreach(($type == 'Post' ? $_POST : $_GET) as $key => $value)
        {
            echo "/**<br /> * @$type()<br /> */<br />$accessType \$$key; //$value<br />";
        }

        die;
    }
}