<?php

namespace Orgup\Expansions;
use Orgup\Application\ConfigLoader;

class AjaxConfigLoader extends ConfigLoader {
    const ROUTING_FILE = 'configs/ajaxrouting.yml';
}