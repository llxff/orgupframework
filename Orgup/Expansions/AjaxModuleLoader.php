<?php
namespace Orgup\Expansions;
use Orgup\Application\ModuleLoader;
use Orgup\Application\Exception\Module\E404;

class AjaxModuleLoader extends ModuleLoader {

    protected $defaultDataClass = 'Orgup\DataModels\AjaxData';

    protected function userHaveRightsForAccessToThisPage()
    {
        return true;
    }
}