<?php

namespace Orgup\Expansions;
use Orgup\Application\Outputer;
use \Orgup\Application\Exception\Config\ConfigIsBreak;
use \Orgup\Application\Logger;
use Orgup\DataModels\IndexData;
use Orgup\Application\Registry;

class IndexOutputer extends Outputer
{
    private $required_styles = array();

    private $required_scripts = array(
        'jquery',
        'core',
        'bootstrap',
        'main'

    );

    public function __construct( IndexTemplator $Templator, IndexData $Data ) {

        parent::__construct($Templator, $Data);

        $Data->initStylesAndScripts();
        $this->initStylesAndScripts();
    }

    public function getOutput()
    {
        Logger::log('Start of Output', __FILE__, __LINE__);


        return $this->renderHeader().
               $this->renderBody().
               $this->renderFooter();

    }

    private function renderHeader()
    {
        $template = $this->getTemplator()->get_header_template();

        if(!empty($template))
        {
            return $this->getTemplateEngine()
                ->loadTemplate( $template )
                    ->render( array(
                                 'data'    => $this->getData(),
                                 'ways'    => $this->getWays(),
                                 'lang'    => $this->getData()->getLang(),
                                 'scripts' => $this->scripts,
                                 'styles'  => $this->styles
                            ) );
        }

        return '';
    }

    private function renderFooter()
    {
        return $this->renderTemplate($this->getTemplator()->get_footer_template());
    }

    private function renderBody()
    {
        return $this->renderTemplate($this->getTemplator()->get_main_template());
    }

    private function initStylesAndScripts()
    {

        if (Registry::instance()->get('debug') ) {
            $this->required_styles[] = 'debug';
            $this->required_scripts[] = 'debug';
        }

        $scripts_keys = $this->mergeKeys( $this->required_scripts, $this->getData()->get_scripts() );
        $styles_keys = $this->mergeKeys( $this->required_styles, $this->getData()->get_styles() );

        // формируем полные массивы путей
        if ( !empty( $scripts_keys ) )
        {
            $this->scripts = $this->loadSettings( $scripts_keys, ROOTDIR.self::SCRIPTS_VERSION, ROOTDIR.self::SCRIPTS );
        }

        if ( !empty( $styles_keys ) )
        {
            $this->styles = $this->loadSettings( $styles_keys, ROOTDIR.self::STYLES_VERSION, ROOTDIR.self::STYLES );
        }
    }
}

