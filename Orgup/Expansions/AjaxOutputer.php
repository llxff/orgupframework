<?php

namespace Orgup\Expansions;
use Orgup\Application\Outputer;

class AjaxOutputer extends Outputer {

    const DEBUG_TEMPLATE = 'main\elements\debuginner.htm';

	public function getOutput() {

        $this->getData()->beforeOutput();

        $jsFilesToLoad      = $this->getData()->get_js();
        $notifications      = $this->getData()->get_notifications();
        $errors             = $this->getData()->get_errors();
        $cacheTime          = $this->getData()->get_cache_time();
        $scripts            = $this->getData()->get_scripts();
        $styles             = $this->getData()->get_styles();

        $scripts = $this->mergeKeys( array(),  $scripts );

		$xml = '<labels>';

        $templates = $this->getData()->get_templates();

		if ( !empty($templates) )
        {
			foreach ( $templates as $templateKey => $template )
            {
				$xml .= '<html key="'.$templateKey.'"><![CDATA['.$this->renderTemplate( $template ).']]></html>';
			}
		}

		if ( !empty( $errors ) )
        {
			foreach ( $errors as $err ) {
				$xml .= '<error><![CDATA['.$err.']]></error>';
			}
		}

		if ( !empty( $notifications ) )
        {
			foreach ( $notifications as $mess ) {
				$xml .=  '<message><![CDATA['.$mess.']]></message>';
			}
		}

        // запускаем нужные функции после получения
		if ( !empty( $jsFilesToLoad ) )
        {
			foreach ( $jsFilesToLoad as $jsFile ) {
				$xml .= '<function><funcname>'.$jsFile['function_name'].'</funcname>';

				if ( isset( $jsFile['parameters'] ) && is_array($jsFile['parameters']))
                {
					foreach ( $jsFile['parameters'] as $paramName => $paramValue )
                    {
                        if ( is_array( $paramValue ) && isset( $paramValue['translate_it'] ) )
                        {
                            if ( $paramValue['translate_it'] ) {
                                $paramValue = $this->translate( $paramValue );
                            }
                            else
                            {
                               $paramValue = $paramValue['param'];
                            }
                        }

                        if(is_array($paramValue) && isset($paramValue['is_array'], $paramValue['parameters']))
                        {
                            $xml .= '<parameter><name>'.$paramName.'</name><value>';
                            $xml .= '<![CDATA['.json_encode($paramValue['parameters']).']]>';
                            $xml .= '</value></parameter>';
                        }
                        else
                        {
                            $xml .= "<parameter><name>$paramName</name><value><![CDATA[$paramValue]]></value></parameter>";
                        }
					}
				}

				$xml .= '</function>';
			}
		}

        // добавляем нужные файлы скриптов на страницу
        if ( !empty( $scripts ) )
        {

            $scriptsPaths = $this->initScripts( $scripts );

            foreach ( $scriptsPaths as $script )
            {
                $xml .= '<script><![CDATA['.$script.']]></script>';
            }
        }

        // добавляем нужные файлы стилей на страницу
        if ( !empty( $styles ) )
        {

            $stlyesPaths = $this->initStyles( $styles );

            foreach ( $stlyesPaths as $style ) {
                $xml .= '<style><![CDATA['.$style.']]></style>';
            }
        }

        if ( $cacheTime !== 0 ) {
            $xml .= '<cachetime>'.$cacheTime.'</cachetime>';
        }

        if ( $this->getData()->getDebug() ) {
            $xml .= '<log><![CDATA['.$this->renderTemplate( self::DEBUG_TEMPLATE ).']]></log>';
        }

		$xml .= '</labels>';

        $this->sendHeaders();
        return $xml;
	}

    private function translate( $parameter ) {

        $lang = $this->getData()
            ->getLang();
        return isset( $lang['ajax'][$parameter['type']][$parameter['param']] ) ?
             $lang['ajax'][$parameter['type']][$parameter['param']] : 'I was not able to translate title';
    }

    private function sendHeaders() {
        header('Pragma: no-cache');
        header('Cache-Control: no-store');
        header('Content-Type: application/xml');
    }

    private function initScripts( $scripts_keys ) {
        return $this->scripts = $this->loadSettings( $scripts_keys, ROOTDIR.self::SCRIPTS_VERSION, ROOTDIR.self::SCRIPTS );
    }

    private function initStyles( $styles_keys ) {
        return $this->styles = $this->loadSettings( $styles_keys, ROOTDIR.self::STYLES_VERSION, ROOTDIR.self::STYLES );
    }
}