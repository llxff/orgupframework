<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 20.03.12
 * Time: 16:58
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Expansions\Module\Filter;
use Orgup\Application\Exception\Module\E404;
use Orgup\Application\Registry;

class WhenDebug extends Filter
{

    public function ifInvalid()
    {
        throw new E404();
    }

    public function isValid()
    {
        $params = $this->getParams();

        if(isset($params['show']) && $params['show'] == true && Registry::instance()->get('debug') == true)
        {
            return true;
        }

        return false;
    }
}
