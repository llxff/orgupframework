<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 12.11.11
 * Time: 12:30
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Expansions\Module\Filter;
use \Orgup\Application\Exception\Module\E404;
use \Orgup\Application\Registry;
use Orgup\Application\HTTPRouting;

class Server extends Filter
{
    const SETTING = 'domain';

    public function isValid()
    {
        $params = $this->getParams();

        $url = $params[self::SETTING];
        $redirect = trim(Registry::instance()->get('Routing')->route_server('HTTP_REFERER'));
        if($redirect != '')
        {
            $redirect = parse_url($redirect);

            if(isset($redirect['host']))
            {
                $domain = explode('.', $redirect['host']);

                $domain = implode('.', array($domain[sizeof($domain) - 2], $domain[sizeof($domain) - 1]));

                if($domain == $url)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public function ifInvalid()
    {
        throw new E404();
    }
}
