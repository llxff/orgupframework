<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 03.11.11
 * Time: 15:36
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Expansions\Module\Output;
use \Orgup\Expansions\Module\ExpansionHandler;

class Handler implements ExpansionHandler
{
    private $options;

    public function __construct(array $options)
    {
        $this->options = $options;
    }

    public function run()
    {


        foreach ($this->options as $option)
        {
            if($this->isBufferedOption($option))
            {
                if($option[ExpansionHandler::PARAM_VALUE])
                {
                    ob_start();
                }
            }
        }
    }

    private function isBufferedOption($option)
    {
        return isset($option[ExpansionHandler::MODULE_NAME]) && strcmp($option[ExpansionHandler::MODULE_NAME], 'buffered') === 0;
    }
}
