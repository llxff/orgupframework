<?php

namespace Orgup\Expansions;
use Orgup\Application\Registry;
use \Orgup\Application\Logger;

interface WaysInterface {
    public function add( $param, $value, $path = null );
    public function delete_param( $param, $path = null );
    public function thispage();
    /** mainpage. example http://localhost/ */
    public function path();
    public function adminpage();
    /**
     * @return string path/?logout=1
     */
    public function logout();
}

class Ways implements WaysInterface {

    protected $host;
    private $domain;

    private $adminMenu;

    public function __construct() {
        Logger::log('Init Ways', __FILE__, __LINE__);
        $this->host = Registry::instance()->get('HOST');
        $this->domain = Registry::instance()->get('Routing')->route_server('HTTP_HOST');

        $this->adminMenu = array(
                    'site' => array(
                        'pages'         => $this->admin_custom_pages()
                    ),
                    'users' => array(
                        'users'         => $this->admin_users_list()
                    ),
                    'site_manager' => array(
                        'site_manager' => $this->admin_site()
                    )
                );
    }

    public function path() {
        return $this->host;
    }

    public function domain()
    {
        return $this->domain;
    }

    public function protocol()
    {
        return 'http://';
    }

    public function thispage() {
        return Registry::instance()->get('Path')->get_full_path();
    }

    public function logout() {
        return $this->add( 'logout', 1 );
    }
    /**
     * @param $param
     * @param $value
     * @param null $path
     * @return mixed
     * @deprecated
     */
    public function add( $param, $value, $path = null ) {
        return Registry::instance()->get('Path')->add_param( $param, $value, $path );
    }

    public static function addParam( $param, $value, $path = null )
    {
        return Registry::instance()->get('Path')->add_param( $param, $value, $path );
    }

    public function delete_param( $param, $path = null  ) {
        return Registry::instance()->get('Path')->delete_param( $param, $path );
    }

    public function adminpage() {
        return $this->host.'administration/';
    }

    /**
     * @return string path?entry=1
     */
    public function entry() {
        return $this->add( 'entry', 1 );
    }

    // ############### COMMON ###################

    public function login() {
        return $this->host.'login/';
    }

    public function login_on_this_page() {
        return $this->add( 'return_to', urlencode( $this->thispage() ), $this->login() );
    }

    // адрес определенной страницы
    public function custom_page_id( $id_page ) {
        return $this->host.'page/'.$id_page;
    }


    // ################# ADMIN COMMON ###################
    public function get_admin_menu_massive() {
        return $this->adminMenu;
    }

	// загрузчик изображений
    public function admin_upload_image() {
        return $this->adminpage().'upload/';
    }

    // подгрузка ckfinder
    public function admin_ckfinder() {
        return $this->adminpage().'ckfinder/?Type=Images';
    }

    public function admin_ckfinder_connector() {
        return $this->adminpage().'connector/';
    }

    // ################# / ADMIN COMMON ###################

	################## CUSTOM PAGE EDITOR ####################
    public function admin_custom_pages() {
        return $this->adminpage().'pages/';
    }

	// удаление страницы
    public function admin_custom_page_delete( $id ) {
        return $this->add( 'delete_page', $id, $this->admin_custom_pages() );
    }

	// редактирование страницы
    public function admin_custom_page( $id ) {
        return $this->admin_custom_pages().$id;
    }

	// новая страница
    public function admin_new_page() {
        return $this->admin_custom_pages().'newpage';
    }
	################## / CUSTOM PAGE EDITOR ####################

    public function admin_user( $id ) {
        return $this->admin_users_list().$id;
    }

    public function admin_user_to_ban( $id ) {
        return $this->add( 'user_to_ban', $id, $this->admin_users_list() );
    }

    public function admin_user_rules( $id ) {
        return $this->adminpage().'rules-'.$id;
    }

    public function admin_users_list() {
        return $this->adminpage().'users/';
    }

    public function admin_user_new()
    {
        return $this->admin_users_list().'new/';
    }

    ################# Управление сайтом ################

    public function admin_site() {
        return $this->adminpage().'site/';
    }

    public function add_to_admin_menu($menu)
    {
        $this->adminMenu = array_merge_recursive( $this->adminMenu, $menu);
    }

    public function add_to_admin_menu_group($group, $menu)
    {
        if ( !isset( $this->adminMenu[$group] ) )
            $this->adminMenu[$group] = array();

        $this->adminMenu[$group] = array_merge($this->adminMenu[$group], $menu);
    }


}

