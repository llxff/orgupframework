<?php

namespace Orgup\Expansions;
use Orgup\Application\Routing;
use Orgup\Application\SpotModule;

class AjaxSpotModule extends SpotModule {

    const ACTION_VALUE = 'ajaxaction';

    private $actionType = array('box' => 'boxtype', 'optionator' => 'type');

    public function __construct( \Orgup\Application\MainLoaderInterface $configLoader, Routing $Routing ) {

        $this->routingRules = $configLoader->getRoutingRules();
        $this->Routing = $Routing;

        $this->ajax_spot_module( $this->routingRules, $this->Routing->route_post( self::ACTION_VALUE ) );
    }

    private function ajax_spot_module( $routing, $module ) {
        
        $moduleName = '_'.$module;
        
        if ( isset( $routing[$moduleName] ) ) {

            if ( isset($this->actionType[$module]) ) {
                if ( isset( $routing[$moduleName] ) && is_array( $routing[$moduleName] ) )
                    $this->ajax_spot_module( $routing[$moduleName], $this->Routing->route_post( $this->actionType[$module] ) );
                return;
            }

            if ( $this->haveExtendsDeclaration( $routing[$moduleName] ) ){
                $this->extendsResourceAndRights( $routing[$moduleName] );
            }

            $this->moduleResource = $routing[$moduleName]['resource'];
            $this->moduleName = $module;

            if ( isset( $routing[$moduleName]['rulesname'] ) )
                $this->moduleRights = $routing[$moduleName]['rulesname'];
        }
    }
}