<?php

namespace Orgup\DataModels\Index\Administration\Site;
use \Orgup\DataModels\Index\Administration\Administration;

class Upload extends Administration {

    private $name;
    private $error = array();

    public function getName() {
        return $this->name;
    }

    public function setName( $name ) {
        $this->name = $name;
    }
}