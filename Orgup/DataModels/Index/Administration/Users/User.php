<?php

namespace Orgup\DataModels\Index\Administration\Users;
use \Orgup\DataModels\Index\Administration\Administration;

class Users extends Administration {

    private $Users = array();

    public function setUsers( array $Users ) {
        $this->Users = $Users;
    }

    public function getUsers() {
        return $this->Users;
    }
}