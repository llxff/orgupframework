<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 09.08.12
 * Time: 18:18
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\DataModels\Index;
use \Orgup\DataModels\IndexData;

class MainPageData extends IndexData
{
    public function initStylesAndScripts() {
        $this->add_style( 'mainpage' );

    }
}
