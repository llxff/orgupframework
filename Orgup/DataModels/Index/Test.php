<?php

namespace Orgup\DataModels\Index;
use \Orgup\DataModels\IndexData;

class Test extends IndexData {

    public function content() {
        return 'hello world! it`s working';
    }

    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    private $messages;
}