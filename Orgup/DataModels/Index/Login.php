<?php

namespace Orgup\DataModels\Index;
use \Orgup\DataModels\IndexData;
use \Orgup\Application\Registry;

class Login extends IndexData {

    public function initStylesAndScripts() {
        $this->add_style('login');
        $this->set_title('login');
        $this->add_script('login');
        $this->run_script('login');
    }

    function __construct()
    {
        parent::__construct();

        $this->AuthMode = Registry::instance()->get('dev') == true ? 'dev' : 'public';
    }

    public function vkKey()
    {
        $settings = Registry::instance()->get('vkontakte');
        return $settings[$this->AuthMode]['app'];
    }

    public function fbKey()
    {
        $settings = Registry::instance()->get('facebook');
        return $settings[$this->AuthMode]['app'];
    }

    public function login() {
        return (string)Registry::instance()->get('Routing')->route_post('login');
    }
}