<?php

namespace Orgup\DataModels\Ajax;
use Orgup\DataModels\AjaxData;

class Box extends AjaxData {

    protected $box_id;
    protected $box_size;

    private $header_text;
    private $_overlay            = false;
    protected $_this_box_closed  = false;
    private   $_translate_header = true;

    public function beforeOutput() {

        $this->box_id = (string)$this->Routing()->route_post('boxid');

        // если окно не закрыто - рендерим
        if ( !$this->_this_box_closed )
            array_unshift( $this->js,
                array( 'function_name' => 'render_box',
                    'parameters' => array(
                        'header' => array(
                             'translate_it' => $this->_translate_header,
                             'type'         => 'boxheaders',
                             'param'        => $this->header_text
                        ),
                        'boxid' => $this->box_id,
                        'boxsize' => $this->box_size,
                        'overlay' => $this->_overlay
                    )
                )
            );
    }

    protected function overlay_on() {
        $this->_overlay = true;
    }

    protected function close_this_box() {
        $this->box_id = (string)$this->Routing()->route_post('boxid');
        $this->add_js('close_box', array('boxid' => $this->box_id ) );
        $this->_this_box_closed = true;
    }

    protected function set_box_size( $size ) {
        $this->box_size = $size;
    }

    public function set_header( $header, $translate_it = true ) {
        $this->header_text = $header;
        $this->_translate_header = $translate_it;
    }

    public function get_header() {
        return $this->header_text;
    }
}