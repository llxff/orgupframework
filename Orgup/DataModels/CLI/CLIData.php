<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 09.04.12
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\DataModels\CLI;
use Orgup\DataModels\Data;
use Orgup\CLIApplication\CLIRuntimeError;

class CLIData extends Data
{
    function __construct($runtimeErrors = array())
    {
        $this->errors = $runtimeErrors;
    }

    public function add_error($error , $notusingparam) {
        $this->add_custom_error($error);
    }

     /**
      * @param $error
      * @return void
      */
     public function add_custom_error(CLIRuntimeError $error)
     {
         $this->errors[] = $error;
     }

     /**
      * @return array
      */
     public function get_errors() {
         return $this->errors;
     }

     /**
      * @param $notification_text
      * @param $module_name
      * @return array
      */
     public function add_notification( $notification_text, $notusingparam ) {
         $this->notifications[] = $notification_text;
     }

     /**
      * @return array
      */
     public function get_notifications() {
         return $this->notifications;
     }

     /**
      * @return bool
      */
     public function imember() {
         return false;
     }

     /**
      * @return \Orgup\Common\User
      */
     public function user() {
         return null;
     }

    public function eol()
    {
        return PHP_EOL;
    }
}
