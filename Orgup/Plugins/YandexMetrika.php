<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 21.10.11
 * Time: 18:55
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Plugins;
use \Orgup\Cache\Cache;
use \Orgup\Application\Exception\HTTP\NotFound;

class YandexMetrika {
    const Api = 'http://api-metrika.yandex.ru/stat/traffic/summary.json?id=2925805&pretty=1&oauth_token=584bdef045f7477cbb14f5ae4b53995a&';

    private $timeMode = 'today';

    private $dateForApi;

    private $data;

    private $Cache;

    private $startDate = 0;
    private $endDate = 0;

    private $dbIsEmpty = false;


    function __construct($timeMode)
    {
        $this->Cache = Cache::instance()->getAnalyticsCon();

        $this->timeMode = $timeMode;
        $this->dateForApi = $this->getDates();

        $this->data = $this->getDataFromApi();

        $this->Cache = null;
    }

    private function getDates()
    {
        $this->checkCache();

        $dates = array();

        $dates[0] = $this->startDate;
        $dates[1] = $this->endDate;

        return $this->toDateParams($dates);
    }


    private function checkCache()
    {
        $result = $this->Cache->find()->sort(array('date' => -1))->limit(1);

        if( ! $result->hasNext())
        {
            $this->startDate = date('Ymd', strtotime("-30 day"));
            $this->endDate = date('Ymd', strtotime("-1 day"));
            $this->dbIsEmpty = true;
        }
        else if ($this->isYesterday($result))
        {
            $this->startDate = $this->endDate = 0;
        }
        else
        {
            $result = iterator_to_array($result);
            $result = array_pop($result);
            $this->startDate = $result['date'];
            $this->endDate = date('Ymd', strtotime("-1 day"));
        }
    }

    private function isYesterday(\MongoCursor $day)
    {
        $day = $day->getNext();

        if($day['date'] == date('Ymd', strtotime("-1 day")))
        {
            return true;
        }

        return false;
    }

    private function toDateParams(array $dates)
    {
        return 'date1='.$dates[0].'&date2='.$dates[1];
    }

    private function getDataFromApi()
    {
        $Data = $this->loadDataFromYandex();
        return $this->getDataFromCache($Data);

    }

    private function loadDataFromYandex()
    {
        $Data = array();

        if(!$this->actualData())
        {
            $ch = curl_init(self::Api.$this->dateForApi);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $result = json_decode(curl_exec($ch), true);

            if(curl_errno($ch) == 401)
            {
                throw new NotFound();
            }

            \Orgup\Application\Logger::err('connect to metrika');

            curl_close($ch);

            $Data = $result['data'];

            $this->Cache->batchInsert($Data);
        }

        return $Data;
    }

    private function getDataFromCache(array $Data)
    {
        $cond = array('date' => array('$gte' => $this->getDate()));
        $fields = array('visitors' => 1, 'date' => 1);
        $sort = array('date'=> -1);

        if(empty($Data))
        {
            $CachedData = array_values(
                iterator_to_array(
                    $this->Cache->find($cond, $fields)->sort($sort)
                )
            );

            return array_reverse($CachedData);
        }
        else if( ! $this->dbIsEmpty )
        {
            $CachedData = array_values(
                iterator_to_array(
                    $this->Cache->find($cond, $fields)->sort($sort)
                )
            );

            return array_reverse(
                array_merge(
                    $Data,
                    $CachedData
                )
            );
        }
        else {
            return array_reverse($Data);
        }
    }

    private function getDate()
    {
        switch($this->timeMode)
        {
            case 'yesterday':
                return date('Ymd',strtotime("-1 day"));
            case 'week':
                return date('Ymd',strtotime("-1 week"));
            case 'month':
                return date('Ymd',strtotime("-30 day"));
            default:
                return date('Ymd',strtotime("-1 day"));
        }
    }



    private function actualData()
    {
        return $this->startDate == $this->endDate && $this->endDate == 0;
    }

    public function getData()
    {
        return $this->data;
    }
}
