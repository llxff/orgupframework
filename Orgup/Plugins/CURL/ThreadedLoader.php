<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 01.02.12
 * Time: 16:11
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Plugins\CURL;
use Orgup\Application\Logger;

class ThreadedLoader
{
    private $groups = array();
    private $api;

    function __construct($groups, $api)
    {
        $this->groups = $groups;
        $this->api = $api;
    }

    public function getContent()
    {
        $cmh = curl_multi_init();

        $tasks = array();

        foreach ($this->groups as $group)
        {
            $ch = $this->createTask($group);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $tasks[$group] = $ch;

            curl_multi_add_handle($cmh, $ch);
        }


        $active = null;

        do {
            $mrc = curl_multi_exec($cmh, $active);
        }
        while ($mrc == CURLM_CALL_MULTI_PERFORM);

        $result = array();

        while ($active && ($mrc == CURLM_OK)) {

            if (curl_multi_select($cmh) != -1) {

                do {
                    $mrc = curl_multi_exec($cmh, $active);

                    $info = curl_multi_info_read($cmh);

                    if ($info['msg'] == CURLMSG_DONE) {
                        $ch = $info['handle'];

                        $group = array_search($ch, $tasks);

                        unset($tasks[$group]);

                        $content = curl_multi_getcontent($ch);

                        curl_multi_remove_handle($cmh, $ch);
                        curl_close($ch);

                        $result[$group] = $content;
                    }
                }
                while ($mrc == CURLM_CALL_MULTI_PERFORM);
            }
        }

        curl_multi_close($cmh);

        return $result;
    }

    protected function createTask($task)
    {
        $url = $this->api . urlencode(trim(urldecode($task)));
        Logger::log($url);

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_HEADER, 0);

        return $ch;
    }

    public function getApi()
    {
        return $this->api;
    }
}
