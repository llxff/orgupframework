<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 24.04.12
 * Time: 16:20
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Plugins\Thread;
use Aza\Components\Autoloader\UniversalClassLoader;

class ThreadSystemLoader
{
    public static function load()
    {

        define('IS_CLI', PHP_SAPI === 'cli');

        /** Windows flag */
        define('IS_WIN', !strncasecmp(PHP_OS, 'win', 3));

        require_once ROOTDIR . 'Orgup/Plugins/Thread/Components/Autoloader/UniversalClassLoader.php';

        $autoloader = new UniversalClassLoader();
        $autoloader->registerNamespaces(array(
            'Aza' =>ROOTDIR . 'Orgup/Plugins/Thread' . DIRECTORY_SEPARATOR,
        ));
        $autoloader->register();
    }
}
