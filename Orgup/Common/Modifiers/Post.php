<?php

namespace Orgup\Common\Modifiers;

use \Orgup\Common\Modifier;
use Orgup\Application\Registry;

class Post extends Modifier
{
    public $name;
    public $required = true;
    public $is_array = false;
    public $is_checkbox = false;
    public $can_be_empty = true;
    public $default = '';
    public $file = false;

    protected $errorType = 'Post';


    protected function createResult()
    {
        if($this->file)
        {
            $this->is_array = true;
        }

        if($this->name == null)
        {
            $this->name = $this->property_name;
        }

        $routingValue = $this->routeParam($this->name);
        
        if($this->is_checkbox == true)
        {
            return $routingValue === 'on';
        }
        else if($this->is_array && is_array($routingValue ))
        {
            return $routingValue;
        }
        else if($routingValue !== null && !$this->is_array && !is_array($routingValue))
        {
            if(!$this->can_be_empty && $routingValue == '')
            {
                return $this->value;
            }

            return $routingValue;

        }
        else if(!$this->required || !$this->can_be_empty)
        {
            if(!$this->suppress_warnings)
            {
                $this->object->set_error($this->errorType, $this->name);
            }

            $method = explode('|', $this->default);
            if(count($method) > 1 && $method[0] == 'm')
            {
                return eval('return '.$method[1].';');
            }
            else
            {
                if($this->default == '')
                {
                    return $this->value;
                }
                else
                {
                    return $this->default;
                }
            }
        }
        else
        {
            if(!$this->suppress_warnings)
            {
                $this->object->set_error($this->errorType, $this->name);
            }

            return $this->value;
        }
    }

    protected function routeParam($name)
    {
        if($this->file)
        {
            return Registry::instance()->get('Routing')->route_files($name);
        }

        return Registry::instance()->get('Routing')->route_post($name);
    }
}