<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 14.02.12
 * Time: 19:01
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\Modifiers;
use Orgup\Application\Registry;
use Orgup\Common\Modifiers\Request\RequestIsset AS VarIsset;

class RequestIsset extends VarIsset
{
    protected function getParams()
    {
        return Registry::instance()->get('Routing')->request();
    }
}
