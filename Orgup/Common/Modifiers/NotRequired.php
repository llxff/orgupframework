<?php
/**
 * Created by JetBrains PhpStorm.
 * User: FMS-A1
 * Date: 31.07.12
 * Time: 17:30
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\Modifiers;
use \Orgup\Common\Modifier;

class NotRequired extends Modifier
{
    public $removeFrom = '';

    protected function createResult()
    {
        $value = trim($this->object->originalsArray()[$this->property_name]);

        if(!$value)
        {
            $this->object->removeFromValidateError($this->property_name);

            if($this->removeFrom !== '')
            {
                $this->object->removeFromError($this->removeFrom, $this->property_name);
            }
        }

        return $this->value;
    }
}
