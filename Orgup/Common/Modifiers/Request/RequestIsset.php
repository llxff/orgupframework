<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 14.02.12
 * Time: 18:59
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\Modifiers\Request;
use Orgup\Application\Registry;
use Orgup\Common\Modifier;

abstract class RequestIsset extends Modifier
{
    public $fields;
    public $one_enough = false;

    protected function createResult()
    {
        $fields = explode('|', $this->fields);
        $params = $this->getParams();

        foreach($fields as $field)
        {
            $currentExists = isset($params[$field]);

            if($this->one_enough && $currentExists) return true;

            if(!$this->one_enough && !$currentExists) return false;
        }

        if($this->one_enough)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    protected abstract function getParams();
}
