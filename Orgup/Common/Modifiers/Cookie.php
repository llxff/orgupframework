<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 05.01.12
 * Time: 17:22
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\Modifiers;
use Orgup\Application\Registry;

class Cookie extends Post
{
    protected $errorType = 'Cookie';

    protected function routeParam($name)
    {
        return Registry::instance()->get('Routing')->route_cookie($name);
    }
}
