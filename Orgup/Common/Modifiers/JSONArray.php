<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 09.02.12
 * Time: 16:34
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\Modifiers;

class JSONArray extends Post
{
    public $keys = '';

    protected function createResult()
    {
        $value = json_decode(parent::createResult(), true);

        if($value === null && $this->required)
        {
            $this->addValidateError();
        }

        $keys = explode('|' , $this->keys);

        if(empty($keys))
        {
            return $value;
        }
        else if(is_array($value))
        {
            $validKeys = array();

            foreach($value as $keyValue)
            {
                $extractedKeys = $this->extractKeys($keys, $keyValue);

                $diff = array_diff($keys, array_keys($extractedKeys));

                if(!empty($diff))
                {
                    $this->addValidateError();
                }
                else
                {
                    $validKeys[] = $extractedKeys;
                }
            }

            return $validKeys;
        }

        return array();

    }

    private function extractKeys($keys, $destArray)
    {
        return array_intersect_key($destArray, array_fill_keys($keys, null));
    }
}
