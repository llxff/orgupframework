<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 09.07.12
 * Time: 13:08
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\Modifiers;
use \Orgup\Common\Modifier;

class Value extends Modifier
{
    protected function createResult()
    {
        return $this->value;
    }
}
