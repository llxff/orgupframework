<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 14.02.12
 * Time: 18:54
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\Modifiers;
use Orgup\Application\Registry;

class Request extends Post
{
    protected $errorType = 'Request';

    protected function routeParam($name)
    {
        return Registry::instance()->get('Routing')->route_request($name);
    }
}
