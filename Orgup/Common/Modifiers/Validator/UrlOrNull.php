<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 01.03.12
 * Time: 14:41
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\Modifiers\Validator;
class UrlOrNull extends Url
{
    protected static function isValid()
    {
        if(trim(static::$value) == '') return true;

        return parent::isValid();
    }
}
