<?php
/**
 * User: ilya
 * Date: 22.09.11
 * Time: 12:00
 */
namespace Orgup\Common\Modifiers\Validator;

class Color extends Validator{

    protected static function isValid()
    {
        return static::$value == '' || preg_match('/^[0-9a-fA-F]{6}$/', static::$value );
    }
}
