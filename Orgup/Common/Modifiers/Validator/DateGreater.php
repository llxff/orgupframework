<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 10.02.12
 * Time: 14:47
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\Modifiers\Validator;

class DateGreater extends Validator
{
    protected static function isValid()
    {
        if(preg_match('/^{{(\w+)}}$/', static::$between, $matches))
        {
            $value = $matches[1];
            $properties = static::$Mod->asArray();
            $value = $properties[$value];

            if(static::$value)
            {
                return static::$value >= $value;
            }

            if(!static::$value) return true;

            return false;
        }

        return false;
    }
}
