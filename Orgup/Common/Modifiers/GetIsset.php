<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 20.01.12
 * Time: 13:57
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\Modifiers;
use Orgup\Application\Registry;
use Orgup\Common\Modifiers\Request\RequestIsset;

class GetIsset extends RequestIsset
{
    protected function getParams()
    {
        return Registry::instance()->get('Routing')->get();
    }
}
