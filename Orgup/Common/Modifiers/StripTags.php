<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 06.03.12
 * Time: 17:43
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\Modifiers;

use \Orgup\Common\Modifier;

class StripTags extends Modifier
{
    protected function createResult()
    {
        return strip_tags($this->value);
    }
}
