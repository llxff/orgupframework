<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 13.02.12
 * Time: 12:34
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common;
use Orgup\Application\Registry;
use Orgup\Common\DataStorage\DataStorage;
use Orgup\Common\DataStorage\RegisteredDataStorage;
use Orgup\Common\DataStorage\UnregisteredDataStorage;

class UserDataStorage
{
    /**
     * @var DataStorage
     */
    private static $instance;
    /**
     * @static
     * @return DataStorage
     */
    public static function instance()
    {
        if(self::$instance === null)
        {
            $User = Registry::instance()->User();

            if($User->imember())
            {
                return self::$instance = static::getForRegistered($User);
            }

            return self::$instance = static::getForUnregistered($User);
        }

        return self::$instance;
    }

    public static function getForRegistered($User)
    {
        return new RegisteredDataStorage($User);
    }

    public static function getForUnregistered($User)
    {
        return new UnregisteredDataStorage($User);
    }

}
