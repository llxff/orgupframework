<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 12.11.11
 * Time: 15:47
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common;
use \Orgup\Application\Registry;
use Orgup\Common\DBAccess\DBAccess;
use Orgup\CustomApplication\CustomUser;

class AddUser extends DBAccess
{
    public static function add($data = [])
    {
        if(!isset($data['username']))
        {
            $data['username'] =  microtime();
        }

        self::sGetDB()->insert('users', $data);

        return new CustomUser(self::sGetDB()->lastInsertId());
    }
}
