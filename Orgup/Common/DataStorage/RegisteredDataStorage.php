<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 13.02.12
 * Time: 13:01
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\DataStorage;
use Orgup\Common\DBAccess\DBAccess;
use Orgup\Common\User;

class RegisteredDataStorage extends DBAccess implements DataStorage
{
    private $userId;

    function __construct(User $User)
    {
        $this->userId = $User->getId();
    }

    public function put($key, array $values, $clearOld = true)
    {
        if($clearOld) $this->clear($key);

        return $this->insert($key, array_unique($values));
    }

    public function clear($key)
    {
        return $this->getDB()->delete($this->getTableNameByKey($key), array('user_id' => $this->userId));
    }

    public function get($key)
    {
        $result = $this->getDB()->fetchAll('SELECT `value` FROM '.$this->getTableNameByKey($key).' WHERE user_id = ?', array($this->userId));

        $values = array();

        array_walk($result, function($item) use (&$values) {
            $values[] = $item['value'];
        });

        return $values;
    }

    private function getTableNameByKey($key)
    {
        return "storage_$key";
    }

    private function insert($key, $valuesToInsert)
    {
        if(!empty($valuesToInsert))
        {
            $sql = 'INSERT IGNORE INTO '.$this->getTableNameByKey($key) . ' (user_id, value) VALUES ';
            $values = array_fill(0, sizeof($valuesToInsert), "($this->userId, ?)");
            $sql .= implode(', ', $values);
            return $this->getDB()->executeQuery($sql, array_values($valuesToInsert));
        }

        return true;
    }
}