<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 13.02.12
 * Time: 13:05
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\DataStorage;
use Orgup\Common\DBAccess\DBAccess;
use Orgup\Common\User;

class UnregisteredDataStorage extends DBAccess implements DataStorage
{
    private $userId;

    /**
     * @var \MongoCollection
     */
    private $Collection;

    function __construct(User $User)
    {
        $this->userId = $User->getUniqueDBID();
        $this->Collection = $this->getMongo('user_storage');
    }

    /**
     * @return \MongoCollection
     */
    private function getCollection()
    {
        return $this->Collection;
    }

    public function put($key, array $values, $clearOld = true)
    {
        if($clearOld) $this->clear($key);

        if(!empty($values))
        {
           $mongoValues = array();

           foreach($values as $value)
           {
               $mongoValues[] = array('type' => $key, 'id_user' => $this->userId, 'value' => $value);
           }

           return $this->getCollection()->batchInsert($mongoValues);
        }

        return true;

    }

    public function clear($key)
    {
        return $this->getCollection()->remove(array('type' => $key, 'id_user' => $this->userId));
    }

    /**
     * @param $key
     * @return \MongoCursor
     */
    public function get($key)
    {
        $result = $this->getCollection()->find(array('type' => $key, 'id_user' => $this->userId));
        $values = array();

        while($result->hasNext())
        {
            $value = $result->getNext();
            $values[] = $value['value'];
        }

        return $values;
    }
}
