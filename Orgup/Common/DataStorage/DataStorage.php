<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 13.02.12
 * Time: 13:02
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\DataStorage;

interface DataStorage
{
    public function put($key, array $values, $clearOld = true);
    public function clear($key);
    public function get($key);
}
