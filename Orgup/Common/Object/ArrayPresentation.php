<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 14.01.12
 * Time: 16:42
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\Object;

interface ArrayPresentation
{
    public function asArray();
}
