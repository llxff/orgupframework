<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 25.11.11
 * Time: 14:27
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\DBAccess;
use \Orgup\Application\Exception\DBAccess\FromNotInitialized;
use \Orgup\Common\String;

abstract class DataGateway extends DBAccess
{
    const ASC_ORDER = 'ASC';
    const DESC_ORDER = 'DESC';

    const WHERE_MORE = '<';
    const WHERE_LESS = '>';
    const WHERE_EQUALS = '=';
    const WHERE_LIKE = ' LIKE ';
    const WHERE_NOT_EQUALS = ' !=';

    const FETCH_ASSOC = 1;
    const FETCH_ALL = 2;

    private $limit = array();
    private $orderBy = array();
    private $where = array();
    private $groupBy = array();
    private $from;
    private $sql;
    private $fields = array();

    private $baseSQL;

    private $count;

    /**
     * @param $start
     * @return DataGateway
     */
    public function start($start)
    {
        $this->limit['start'] = $start;
        return $this;
    }

    /**
     * @param $limit
     * @return DataGateway
     */
    public function limit($limit)
    {
        $this->limit['limit'] = $limit;

        return $this;
    }
    /**
     * @param $field
     * @param string $type
     * @return DataGateway
     */
    public function orderBy($field, $type = DataGateway::DESC_ORDER)
    {
        $orderBy = array();


        $orderBy['field'] = $field;
        $orderBy['type'] = $type;

        $this->orderBy[] = $orderBy;

        return $this;
    }

    public function clearOrderBy()
    {
        $this->orderBy = array();
        return $this;
    }

    /**
     * @param $field
     * @param $need
     * @param string $where
     * @return DataGateway
     */
    public function where($field, $need, $where = DataGateway::WHERE_EQUALS, $quote = true)
    {
        if(is_array($need))
        {
            throw new \RuntimeException('need can not be array');
        }

        $this->where['field'][] = $field;
        $this->where['need'][] = $need;
        $this->where['where'][] = $where;
        $this->where['quote'][] = $quote;

        return $this;
    }
    /**
     * @param $from
     * @return DataGateway
     */
    protected function from($from, $as = null, $appendQuotes = true)
    {
        if($appendQuotes)
        {
            $from = '`'.$from.'`';
        }

        $this->from = $from;


        if($as !== null)
        {
            $this->from .= ' AS '.$as. ' ';
        }

        return $this;
    }

    public function groupBy($field)
    {
        $this->groupBy[] = $field;
    }

    protected function sql($sql)
    {
        $this->baseSQL = $sql;
    }

    public function resultSQL()
    {
        return $this->sql;
    }

    public function table()
    {
        return $this->from;
    }

    public function addField($field)
    {
        $this->fields[] = $field;
        return $this;
    }

    protected function addFieldString($string)
    {
        array_merge($this->fields, explode(',', $string));
        return $this;
    }

    public function find($fetchMode = self::FETCH_ALL)
    {
        if($this->mapperData === null)
        {
            if($this->from === null && $this->baseSQL === null) {
                throw new FromNotInitialized();
            }

            if($this->baseSQL === null)
            {
                $fields = '*';
                if(!empty($this->fields))
                {
                    $fields = implode(',', $this->fields);
                }
                $this->sql = new String("SELECT $fields FROM $this->from ");
            }
            else
            {
                $this->sql = new String($this->baseSQL);
            }

            $this->whereSQL($this->sql);
            $this->groupBySql($this->sql);
            $this->orderBySql($this->sql);

            $this->limitSql($this->sql);

            $stmt = $this->getDB()->prepare($this->sql);

            $this->whereParam($stmt);

            $stmt->execute();

            $this->setData($this->fetch($stmt, $fetchMode));
        }

        return $this->mapperData;
    }

    private function fetch(\Doctrine\DBAL\Driver\Statement $Statement, $mode)
    {
        switch($mode)
        {
            case self::FETCH_ALL: return $Statement->fetchAll(\PDO::FETCH_ASSOC);
            case self::FETCH_ASSOC: return $Statement->fetch(\PDO::FETCH_ASSOC);

            default:
                return $Statement->fetchAll(\PDO::FETCH_ASSOC);
        }
    }


    private function whereSQL(String &$sql)
    {
        if(!empty($this->where))
        {
            $sql->append(' WHERE ');

            $whereCount = sizeof($this->where['field']);

            for($i = 0; $i < $whereCount; $i++)
            {
                $field = $this->where['field'][$i];

                if($this->where['quote'][$i])
                {
                    $field = '`'.$field.'`';
                }

                $sql->append($field.$this->where['where'][$i].' :need'.$i.' ');

                if($i != $whereCount - 1)
                {
                    $sql->append(' AND ');
                }
            }
        }
    }

    private function whereParam( \Doctrine\DBAL\Driver\Statement &$stmt)
    {
        if(!empty($this->where))
        {
            $whereCount = sizeof($this->where['field']);

            for($i = 0; $i < $whereCount; $i++)
            {
                $need = $this->where['need'][$i];

                if($this->where['where'][$i] == DataGateway::WHERE_LIKE)
                {
                    $need = '%'.$need.'%';
                }

                $stmt->bindValue('need'.$i, $need);
            }
        }
    }

    private function orderBySql(String &$sql)
    {
        if(!empty($this->orderBy))
        {
            $sql->append(' ORDER BY ');
            $orderBy = array();
            foreach($this->orderBy as $order)
            {
                $orderBy[] = '`'.$order['field'].'` '.$order['type'];
            }

            $sql->append(implode(',', $orderBy) . ' ');
        }
    }

   private function groupBySql(String &$sql)
    {
        if(!empty($this->groupBy))
        {
            $sql->append(' GROUP BY '.implode(',', $this->groupBy). ' ');
        }
    }

    private function limitSql(String &$sql)
    {
        if(!empty($this->limit))
        {
            $sql->append(' LIMIT ');
            if($startIsset = isset($this->limit['start']))
            {
                $sql->append($this->limit['start']);
            }

            if(isset($this->limit['limit']))
            {
                if($startIsset)
                {
                    $sql->append(',');
                }

                $sql->append($this->limit['limit']);
            }
        }
    }

    public function countOfAll()
    {
        if($this->count === null)
        {
            $groupByFields = '';

            if(!empty($this->groupBy))
            {
                $groupByFields .= ',' . implode(',', $this->groupBy);
            }

            $sql = new String("SELECT COUNT(*) as cnt $groupByFields FROM $this->from ");
            $this->whereSQL($sql);
            $this->groupBySql($sql);
            $stmt = $this->getDB()->prepare($sql);
            $this->whereParam($stmt);
            $stmt->execute();
            $count = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            if(!empty($this->groupBy))
            {
                $this->count = $count;
            }
            else
            {
                $this->count = $count[0]['cnt'];
            }
        }

        return $this->count;
    }

    public function result()
    {
        return $this->find();
    }

    public function asArray()
    {
        return $this->result();
    }

    public function isLoaded()
    {
        return $this->mapperData !== null;
    }

    public function insert($data)
    {
        return $this->getDB()->insert($this->table(), $data);
    }

    public function iterator() {}

    public function isFoundedAnything()
    {
        return $this->isLoaded() && $this->isDataExists();
    }

    public function inValuesStatement($sizeOfElements)
    {
        return  implode(',', array_fill(0, $sizeOfElements, '?'));
    }

    public static function setTableIdAsKeyIn(array $array, $key)
    {
        $newArray = [];

        foreach($array as $value)
        {
            $newArray[$value[$key]] = $value;
        }

        return $newArray;
    }
}
