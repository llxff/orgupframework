<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 26.09.12
 * Time: 11:46
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common\DBAccess;

abstract class DataMapper implements \ArrayAccess
{
    protected $mapperData;

    public function setData($data)
    {
        return $this->mapperData = $data;
    }

    public function offsetSet($offset, $value) {

        if(is_null($offset))
        {
            $this->mapperData[] = $value;
        }
        else
        {
            $this->mapperData[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->mapperData[$offset]);
    }

    /**
     * @return bool Can not unset offset
     */
    public function offsetUnset($offset) {
        return false;
    }

    /**
     * @param mixed $offset
     * @return mixed
     * @throws \OutOfBoundsException
     */
    public function offsetGet($offset) {
        if(isset($this->mapperData[$offset]))
        {
            return $this->mapperData[$offset];
        }
        else
        {
            throw new \OutOfBoundsException;
        }
    }

    public function asArray()
    {
        return $this->mapperData;
    }

    public function isDataExists()
    {
        return !empty($this->mapperData);
    }
}
