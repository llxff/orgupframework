<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 12.11.11
 * Time: 15:33
 * To change this template use File | Settings | File Templates.
 */
namespace Orgup\Common;
use \Orgup\Application\Registry;
use Orgup\CustomApplication\CustomUser;
use Orgup\Common\DBAccess\DBAccess;

class ExistsUser extends DBAccess
{
    private $userName;
    private $userInfo;

    function __construct($username, $type = 'username', $additional = array())
    {
        $this->userName = $username;
        $this->userInfo = $this->getDB()->fetchAssoc('SELECT * FROM `users` WHERE `'.$type.'` = ? AND active = "1" LIMIT 1', array($this->userName));
    }

    public function exists()
    {
        return !empty($this->userInfo);
    }

    public function getUser()
    {
        return new CustomUser($this->userInfo['id_user']);
    }

    public function equals($type, $value)
    {
        return (isset($this->userInfo[$type]) && $this->userInfo[$type] == $value);
    }
}
