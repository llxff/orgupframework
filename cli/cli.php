<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 09.04.12
 * Time: 17:11
 * To change this template use File | Settings | File Templates.
 */

use Orgup\Application\Logger;
use Orgup\CLIApplication\CLIConfigLoader;
use Orgup\Application\Runner\CLI;


define( 'ROOTDIR', realpath( __DIR__.'/../' ).'/' );

require ROOTDIR.'Orgup/Application/ClassLoader.php';

$classLoader = new Orgup\Application\ClassLoader('Orgup', ROOTDIR );
$classLoader->register();


Logger::log('Create Application');
$app = new CLI(new CLIConfigLoader());
$app->run();
exit($app->exitCode());