<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 22.05.12
 * Time: 18:56
 * To change this template use File | Settings | File Templates.
 */
namespace OrgupTest;
use Orgup\Application\Routing;

class TestRouting extends Routing
{
    private $settedCookie = array();
    private $unsettedCookie = array();

    public function unset_cookie( $cookie_name ) {
        $this->unsettedCookie[] = $cookie_name;
    }

    public function setcookie( $name, $value, $time = 0 ) {
        $this->settedCookie[$name] = $value;
    }

    public function unset_cookies()
    {
        return $this->unsettedCookie;
    }

    public function reset()
    {
        $this->get = $_GET;
        $this->post = $_POST;
        $this->server = $_SERVER;
        $this->cookie = $_COOKIE;
        $this->files = $_FILES;
    }

    public function set_post($key, $value)
    {
        $this->post[$key] = $value;
    }

    public function set_get($key, $value)
    {
        $this->get[$key] = $value;
    }

    public function set_cookie($key, $value)
    {
        $this->cookie[$key] = $value;
    }
}