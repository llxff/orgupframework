<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 22.05.12
 * Time: 19:37
 * To change this template use File | Settings | File Templates.
 */
namespace OrgupTest\Runner;
use Orgup\Application\Runner\IndexApplication;
use Orgup\Application\SpotModule;
use Orgup\Application\Path;
use Orgup\Application\ConfigLoader;
use Orgup\Application\Registry;
use Orgup\CustomApplication\CustomUser;

class IndexApplicationRunner extends IndexApplication
{
    use ModuleRunnerTrait;

    function __construct()
    {
        parent::__construct(new ConfigLoader());
    }

    protected function getSpotModule()
    {
        return new SpotModule( $this->getConfigLoader(), new Path($this->getLoadWay()) );
    }

    public function reInitUser()
    {
        Registry::instance()->set('User', new CustomUser );
    }
}
