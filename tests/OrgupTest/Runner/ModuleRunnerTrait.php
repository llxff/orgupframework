<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 22.05.12
 * Time: 19:17
 * To change this template use File | Settings | File Templates.
 */
namespace OrgupTest\Runner;
use Orgup\Application\Redirect;

trait ModuleRunnerTrait
{
    private $way = '/';

    public function run()
    {
        $this->beforeRun();

        try
        {
            $this->runApplication();
        }
        catch ( Redirect $e )
        {

            $this->endOfApplication();
            throw new Redirect($e->redirect_to());
        }

        $this->endOfApplication();
    }



    public function runOnWay($way)
    {
        $this->way = $way;
    }

    public function getLoadWay()
    {
        return $this->way;
    }
}
