<?php
namespace OrgupTest\TestUtils\Tickets;
use Orgup\Common\DBAccess\DBAccess;
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 18.04.12
 * Time: 15:49
 * To change this template use File | Settings | File Templates.
 */
class TicketsTypesTestHelper extends DBAccess
{
    const TABLE_NAME = 'ticket_types';
    const COLUMN_EVENT = 'event_id';
    const COLUMN_QUANTITY = 'quantity';
    const COLUMN_COST = 'ticket_cost';

    public static function clear()
    {
        self::sGetDB()->executeQuery('TRUNCATE ' . self::TABLE_NAME);
    }

    public static function add(array $ticket)
    {
        self::sGetDB()->insert(self::TABLE_NAME, $ticket);
        return self::sGetDB()->lastInsertId();
    }
}
