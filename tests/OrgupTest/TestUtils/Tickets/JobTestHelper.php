<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 03.05.12
 * Time: 17:17
 * To change this template use File | Settings | File Templates.
 */
namespace OrgupTest\TestUtils\Tickets;
use Orgup\Plugins\Tickets\Buying\Job;

class JobTestHelper extends Job
{
    public static function clear()
    {
        self::sGetDB()->executeQuery(sprintf('TRUNCATE %s', Job::TABLE_NAME));
    }

    public static function getOfTransaction($transactionId)
    {
        return self::sGetDB()->fetchAssoc(
            sprintf('SELECT * FROM %s WHERE %s = ?',
                Job::TABLE_NAME,
                Job::COLUMN_TRANSACTION
            ),
            array($transactionId)
        );
    }
}
