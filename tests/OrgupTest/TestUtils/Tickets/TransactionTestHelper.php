<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 02.05.12
 * Time: 16:06
 * To change this template use File | Settings | File Templates.
 */
namespace OrgupTest\TestUtils\Tickets;
use Orgup\Plugins\Tickets\Buying\Transaction\TransactionSQLHelper;

class TransactionTestHelper extends TransactionSQLHelper
{
    public static function add(array $transaction)
    {
        self::sGetDB()->insert(TransactionSQLHelper::TABLE_NAME, $transaction);

        return self::sGetDB()->lastInsertId();
    }

    public static function clear()
    {
        self::sGetDB()->executeQuery(sprintf('TRUNCATE %s', TransactionSQLHelper::TABLE_NAME));
    }

    public static function get($id, $keys = array('*'))
    {
        return self::sGetDB()->fetchAssoc(
            sprintf('SELECT %s FROM %s WHERE %s = ?',
                implode(',', $keys),
                TransactionSQLHelper::TABLE_NAME,
                TransactionSQLHelper::COLUMN_TRANSACTION_ID
            ),
            array($id)
        );
    }
}
