<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 02.05.12
 * Time: 12:52
 * To change this template use File | Settings | File Templates.
 */
namespace OrgupTest\TestUtils\Tickets;
use Orgup\Common\DBAccess\DBAccess;

class PurchasedTicketsTestHelper extends DBAccess
{
    const TABLE_NAME = 'purchased_tickets';
    const COLUMN_TICKET_ID = 'ticket_id';
    const COLUMN_EVENT = 'event_id';
    const COLUMN_TICKET_TYPE_ID = 'ticket_type_id';
    const COLUMN_USER_ID = 'id_user';
    const COLUMN_UID = 'uid';
    const COLUMN_KEY = 'ticket_key';
    const COLUMN_COST = 'cost';
    const COLUMN_TRANSACTION_ID = 'transaction_id';

    public static function add(array $ticket)
    {
        self::sGetDB()->insert(self::TABLE_NAME, $ticket);
        return self::sGetDB()->lastInsertId();
    }

    public static function clear()
    {
        self::sGetDB()->executeQuery(sprintf('TRUNCATE %s', self::TABLE_NAME));
    }
}
