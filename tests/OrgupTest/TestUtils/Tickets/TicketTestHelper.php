<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 03.05.12
 * Time: 16:57
 * To change this template use File | Settings | File Templates.
 */
namespace OrgupTest\TestUtils\Tickets;
use Orgup\Plugins\Tickets\RealTicket\Ticket;
class TicketTestHelper extends Ticket
{
    public static function getInTransaction($transactionId)
    {
        return self::sGetDB()->fetchAll(
            sprintf('SELECT * FROM %s WHERE %s = ?',
                Ticket::TABLE_NAME,
                Ticket::COLUMN_TRANSACTION_ID
            ),
            array($transactionId)
        );
    }

    public static function clear()
    {
        self::sGetDB()->executeQuery(sprintf('TRUNCATE %s', Ticket::TABLE_NAME));
    }

    public static function add($data)
    {
        self::sGetDB()->insert(Ticket::TABLE_NAME, $data);
        return self::sGetDB()->lastInsertId();
    }
}
