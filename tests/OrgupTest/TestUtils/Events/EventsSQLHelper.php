<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 18.04.12
 * Time: 14:35
 * To change this template use File | Settings | File Templates.
 */
namespace OrgupTest\TestUtils\Events;
use Orgup\Common\DBAccess\DBAccess;

class EventsSQLHelper extends DBAccess
{
    const TABLE_NAME = '`events`';
    const COLUMN_SELLING_ENABLED = 'selling_enabled';


    public static function clear()
    {
        self::sGetDB()->executeQuery('TRUNCATE '.self::TABLE_NAME);
    }

    public static function add(array $items)
    {
        self::sGetDB()->insert(self::TABLE_NAME, $items);
        return self::sGetDB()->lastInsertId();
    }
}
