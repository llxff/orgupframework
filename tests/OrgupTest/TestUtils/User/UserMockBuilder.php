<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 26.12.11
 * Time: 16:26
 * To change this template use File | Settings | File Templates.
 */
namespace OrgupTest\TestUtils\User;
use Orgup\Application\Registry;
use Orgup\Common\DBAccess\DBAccess;
use Orgup\CustomApplication\CustomUser;

class UserMockBuilder extends DBAccess
{

    private $Test;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $User;
    private $functions;

    function __construct(\PHPUnit_Framework_TestCase $Test, array $functions)
    {
        $this->Test = $Test;
        $this->functions = $functions;
        $this->User = $this->create();
    }

    public function setFunc($name, $return)
    {
        $this->User->expects($this->Test->any())->method($name)->will($this->Test->returnValue($return));
        return $this;
    }

    public function create()
    {
        $this->User = $this->Test->getMock('Orgup\CustomApplication\CustomUser', $this->functions);
        Registry::instance()->set('User', $this->User);
        return $this->User;
    }

    public function getUser()
    {
        return $this->User;
    }

    public function toDefault()
    {
        Registry::instance()->set('User', new CustomUser());
    }

    function __destruct()
    {
        $this->toDefault();
    }

    public static function add(array $data = array())
    {
        if(!isset($data['username']))
        {
            $data['username'] = time();
        }

        self::sGetDB()->insert('users', $data);

        return self::sGetDB()->lastInsertId();
    }
}
