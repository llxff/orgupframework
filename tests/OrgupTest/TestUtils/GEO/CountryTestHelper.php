<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 22.05.12
 * Time: 17:26
 * To change this template use File | Settings | File Templates.
 */
namespace OrgupTest\TestUtils\GEO;
use Orgup\Common\DBAccess\DBAccess;

class CountryTestHelper extends DBAccess
{
    public static function clear()
    {
        self::sGetDB()->executeQuery('TRUNCATE country');
    }
}
