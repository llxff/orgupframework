<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 13.12.11
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
use Orgup\Common\DBAccess\DBAccess;
use Orgup\Application\ConfigLoader;

define( 'ROOTDIR', realpath( __DIR__.'/../' ).'/' );
define( 'ORGUP_TEST', 'On');

require ROOTDIR.'Orgup/Application/ClassLoader.php';
require_once 'PHPUnit/Autoload.php';

$classLoader = new Orgup\Application\ClassLoader('Orgup', ROOTDIR );
$classLoader->register();

$classLoader = new Orgup\Application\ClassLoader('OrgupTest', ROOTDIR.'tests/' );
$classLoader->register();

new ConfigLoader();

function truncateTables()
{
    $tables = DBAccess::sGetDB()->fetchAll(
        'SELECT TABLE_NAME FROM  `information_schema`.`TABLES` WHERE  `TABLE_SCHEMA` =  ?',
        array(DBAccess::sGetDB()->getDatabase()));

    foreach($tables as $table)
    {
        DBAccess::sGetDB()->executeQuery("TRUNCATE `${table['TABLE_NAME']}`");
    }
}

truncateTables();